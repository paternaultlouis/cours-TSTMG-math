\documentclass[10pt]{article}

%Tapuscrit : Denis Vergès

\usepackage[
	a4paper,
	left=3.5cm, right=3.5cm, top=1.8cm, bottom=2.7cm,
]{geometry}
\usepackage[T1]{fontenc}
\usepackage[utf8]{inputenc}
\usepackage{textcomp} 

\usepackage[french, shorthands=off]{babel}
\usepackage[table]{xcolor}
\usepackage{hyperref}
\hypersetup{%
pdfauthor = {APMEP, modifié par des professeurs de mathématique du lycée Ella Fitzgerald, Saint Romain en Gal},
pdfsubject = {Baccalauréat blanc STMG},
pdftitle = {Baccalauréat blanc STMG — Lycée Ella Fitzgerald — 14 ❤ février 2017},
allbordercolors = white,
pdfstartview=FitH}
\usepackage[np]{numprint}

\usepackage{fourier}
\usepackage[scaled=0.875]{helvet}
\renewcommand{\ttdefault}{lmtt}
\usepackage{amsmath,amssymb}
\usepackage{tabularx}
\usepackage[pdf]{pstricks}
\usepackage{pst-plot,pst-text,pst-tree,pstricks-add}
\renewcommand{\theenumi}{\textbf{\arabic{enumi}}}
\renewcommand{\labelenumi}{\textbf{\theenumi.}}
\renewcommand{\theenumii}{\textbf{\alph{enumii}}}
\renewcommand{\labelenumii}{\textbf{\theenumii.}}
\usepackage{enumitem}
\usepackage{fancyhdr}

\newcounter{exercice}
\newcommand{\exercice}[1]{%
\refstepcounter{exercice}%
\textbf{\textsc{Exercice \theexercice} \hfill #1 points}%
}
\newcounter{annexe}
\newcommand{\annexe}[1]{%
\refstepcounter{annexe}%
\textbf{\textsc{Annexe \theannexe{} (exercice \ref{#1})}}%
}

\begin{document}

\setlength\parindent{0mm}
\rhead{\textbf{Lycée Ella Fitzgerald}}
\lhead{\small Sciences et technologies du management et de la gestion}  
\lfoot{\small{Bac blanc}}
\rfoot{\small{14 février 2017}}
\renewcommand \footrulewidth{.2pt}
\pagestyle{fancy}
\thispagestyle{empty}

\begin{center}{\Large \textbf{\decofourleft~Baccalauréat blanc STMG  --- Lycée Ella Fitzgerald~\decofourright\\14 février 2017 }}
\end{center}
    
\vspace{0,3cm}
 
La calculatrice (conforme à la circulaire \no 99-186 du 16 novembre 1999) est autorisée.

Le candidat est invité à faire figurer sur la copie toute trace de recherche, même incomplète ou non fructueuse, qu'il aura développée.

Il sera tenu compte de la clarté des raisonnements et de  la qualité de la rédaction   dans l'appréciation des copies.

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% D'après Centres étranger, juin 2016, exercice 1
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% Pour compiler :
%$ lualatex $basename
%$ lualatex $basename

\exercice{4}\label{exo:qcm}

\medskip

\begin{em}
Cet exercice est un questionnaire à choix multiples (QCM). Pour chacune des questions ci-dessous,
une seule des réponses est exacte. Pour chaque question, indiquer le numéro de
la question et recopier sur la copie la réponse choisie. Aucune justification n'est demandée.

Chaque réponse correcte rapporte 1 point. Une réponse incorrecte ou une absence de
réponse n'apporte ni ne retire aucun point.
\end{em}

\smallskip

Les deux parties de cet exercice sont indépendantes.

\medskip

\textbf{Partie A}

\medskip

Dans cette partie, on considère la fonction $f$ définie sur $[-6~;~4]$ dont la courbe représentative
$\mathcal{C}_f$ est donnée ci-dessous.

\begin{center}
\psset{unit=.95cm}
\begin{pspicture}(-6,-5)(4,7)
\psgrid[gridlabels=0,subgriddiv=1,gridwidth=0.4pt]
\psaxes[linewidth=1.25pt,labelFontSize=\scriptstyle]{->}(0,0)(-6,-5)(4,7)
\psaxes[linewidth=1.25pt,labelFontSize=\scriptstyle](0,0)(-6,-5)(4,7)
\psplot[plotpoints=4000,linewidth=1.25pt,linecolor=blue]{-6}{4}{x 3 exp 4 mul x dup mul 6 mul add 72 x mul  sub 37 div 1 add}
\psplot[plotpoints=4000,linewidth=1.25pt]{-3}{3}{1 x 2 mul sub}
\uput[l](3.8,2.5){\blue $\mathcal{C}_f$}
\psdots(-1,3)(-2,5)
\uput[dl](-1,3){A}
\uput[ur](-2,5){B}
\uput[ur](-2.5,6){T}
\end{pspicture}
\end{center}

La droite T est la tangente à la courbe $\mathcal{C}_f$ au point A$(-1~;~3)$. Elle passe par le point B$(-2~;~5)$.

\medskip

\begin{enumerate}
\item Le nombre dérivé de $f$ en $- 1$ est égal à

\medskip
\begin{tabularx}{\linewidth}{*{3}{X}}
\textbf{a.~~} $\dfrac{1}{2}$&
\textbf{b.~~} $- 2$&
\textbf{c.~~} $1$
\end{tabularx}
\medskip

\item L'ensemble des solutions de l'inéquation $f'(x) \leqslant 0$ est

\medskip
\begin{tabularx}{\linewidth}{*{3}{X}}
\textbf{a.~~} $[- 6~;~-3] \cup [2~;~4]$&
\textbf{b.~~} $[- 3~;~2]$&
\textbf{c.~~} $[- 6~;~- 5,2] \cup [0,5~;~3,2]$
\end{tabularx}
\medskip

\end{enumerate}

\bigskip

\textbf{Partie B}

\medskip
Dans cette partie, on considère la fonction $g$ définie sur l'intervalle $[-2~;~5]$ par

\[g(x) = - 2x^3 + 3x^2 + 12x\]

et on note $g'$ sa fonction dérivée.

\medskip

\begin{enumerate}
\item Pour tout $x \in  [-2~;~5]$,

\medskip
\begin{tabularx}{\linewidth}{*{3}{X}}
\textbf{a.~~} $g'(x) = - 3x^2 + 2x + 12$&
\textbf{b.~~} $g'(x) = - 6x^2 + 6x + 12$&
\textbf{c.~~} $g'(x) = - 2x^2 + 3x + 12$
\end{tabularx}
\medskip

\item Le maximum de la fonction $g$ sur $[-2~;~5]$ est égal à

\medskip
\begin{tabularx}{\linewidth}{*{3}{X}}
\textbf{a.~~} 20&
\textbf{b.~~} 4&
\textbf{c.~~} $- 115$
\end{tabularx}
\end{enumerate}

\vspace{0,5cm}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% D'après Centres étrangers, juin 2015, exercice 3
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\exercice{6}\label{exo:evolution}
\medskip

Le tableau ci-dessous donne l'évolution de l'indice du nombre annuel d'immatriculations de
voitures neuves équipées d'un moteur diesel de 2001 à 2011, base 100 en 2001.

\begin{center}
\begin{tabularx}{\linewidth}{|l|*{11}{>{\centering \arraybackslash}X|}}\hline
Année 					&2001	&2002	&2003	&2004	&2005&2006&2007&2008&2009&2010&2011\\ \hline
Rang de l'année $x_i$	&0 		&1 		&2 		&3 		&4 &5 &6 &7 &8 &9 &10\\ \hline
Indice $y_i$			&100 	&106,8 	&106,8 	&109,9 	&112,7 &112,6 &120,3 &124,9 &126,0 &122,7 &122,9\\ \hline
\multicolumn{12}{r}{Source: d'après INSEE}
\end{tabularx}
\end{center}

Le nuage des points de coordonnées $\left(x_i~;~y_i\right)$ pour $i$ variant de 0 à 10 est donné en annexe \ref{annexe:evolution} (page \pageref{annexe:evolution}), à rendre avec la copie.

\medskip

\begin{enumerate}
\item 
	\begin{enumerate}
		\item Déterminer, à l'aide du tableau, le taux d'évolution du nombre d'immatriculations de voitures neuves équipées d'un moteur diesel entre 2001 et 2011 exprimé en
pourcentage.
		\item On sait que \np{1268}~milliers de voitures neuves équipées d'un moteur diesel ont été immatriculées en 2001. Calculer le nombre de voitures de ce type immatriculées en
2011.
	\end{enumerate}
\item Calculer le taux d'évolution moyen annuel entre 2009 et 2011, exprimé en pourcentage et
arrondi à 0,01\,\%.
\item  
	\begin{enumerate}
		\item À l'aide de la calculatrice, donner une équation de la droite d'ajustement affine de $y$ en fonction de $x$ obtenue par la méthode des moindres carrés. Les coefficients seront arrondis au
centième.
		\item On décide d'ajuster ce nuage de points par la droite $D$ d'équation $y = 2,5x + 102,6$.
		
Tracer cette droite sur le graphique figurant en annexe \ref{annexe:evolution} (page \pageref{annexe:evolution}).
		\item À l'aide de ce modèle, estimer les indices du nombre de voitures neuves équipées d'un moteur diesel immatriculées en 2012 et en 2013.
	\end{enumerate}

\item Le tableau ci-dessous donne le nombre d'immatriculations de voitures neuves (exprimé en
milliers) équipées d'un moteur diesel de 2009 à 2013.
	
\begin{center}
\begin{tabularx}{\linewidth}{|m{3.5cm}|*{5}{>{\centering \arraybackslash}X|}}\hline	
Année &2009 &2010 &2011 &2012 &2013\\ \hline
Nombre d'immatriculations (en milliers)&\np{1597,7}& \np{1555,4}& \np{1558,2}& \np{1354,9}& \np{1182,2}\\ \hline
Indice $y_i$, base 100 en 2001& 126,0& 122,7& 122,9&& \\ \hline
\end{tabularx}
\end{center}

	\begin{enumerate}
		\item Faut-il remettre en question l'estimation faite à la question 3. c. ?
		\item Si la tendance observée sur le tableau entre 2011 et 2013 se poursuit, combien de
voitures neuves équipées d'un moteur diesel devront être immatriculées en 2015?
Expliquer la démarche entreprise.
	\end{enumerate}
\end{enumerate}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% D'après Antilles, juin 2016, exercice 3
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\exercice{6}\label{exo:proba}

\medskip

Une entreprise familiale fabrique de la confiture de fraises biologiques. Elle achète ses fruits
auprès de deux fournisseurs locaux A et B.

\setlength\parindent{8mm}
\begin{description}
\item[ ] 25\,\% des fruits proviennent du fournisseur A et les autres du fournisseur B.
\item[ ] 95\,\% des fruits provenant du fournisseur A sont retenus pour la fabrication de la confiture.
\item[ ] 80\,\% des fruits provenant du fournisseur B sont retenus pour la fabrication de la confiture.
\end{description}
\setlength\parindent{0mm}

\medskip

\begin{em}
Dans la suite, on notera : $p(E)$ la probabilité d'un évènement $E$ ; pour tout évènement $F$ de probabilité non nulle, $p_F(E)$ la probabilité de l'évènement $E$ sachant que $F$ est réalisé ; et $\overline E$ l'évènement contraire de l'évènement $E$.
\end{em}

\medskip

On choisit un fruit au hasard dans la production.

On note $A$, $B$, $C$ les évènements:

\setlength\parindent{8mm}
\begin{description}
\item[ ]$A$ : \og le fruit provient du fournisseur A \fg
\item[ ]$B$ : \og le fruit provient du fournisseur B \fg
\item[ ]$C$ : \og le fruit est retenu pour la fabrication de la confiture \fg
\end{description}
\setlength\parindent{0mm}

\smallskip
Dans cette partie, les résultats seront arrondis au centième.
\medskip

\begin{enumerate}
\item Déterminer $P_A\left(C\right)$.
\item  En utilisant les données de l'énoncé recopier et compléter l'arbre de probabilités ci-dessous :
\begin{center}
\pstree[treemode=R,nodesep=2.5pt,labelsep=0.1pt]{\TR{}}
{\pstree{\TR{$A$}\taput{0,25}}
	{\TR{$C$}\taput{\dots}
	\TR{$\overline{C}$}\tbput{\dots}
	}
\pstree {\TR{$B$}\tbput{\dots}}
	{\TR{$C$}\taput{0,80}
	\TR{$\overline{C}$}\tbput{\dots}
	}
}
\end{center}
\item  
	\begin{enumerate}
		\item Définir par une phrase l'évènement $A \cap C$.
		\item Calculer $p(A \cap C)$.
		\item Les évènements $A$ et $C$ sont-ils incompatibles ? Interpréter la réponse dans le
contexte de l'exercice.
	\end{enumerate}
\item  
	\begin{enumerate}
		\item Montrer que la probabilité $p(C)$, arrondie au centième, est égale à $0,84$.
\item Calculer la probabilité que le fruit choisi au hasard ne soit pas retenu pour la fabrication de la confiture.
	\end{enumerate}
\item  Calculer $p_C(A)$. Interpréter la réponse dans le contexte de l'exercice.
\end{enumerate}

\vspace{0,5cm}


\pagebreak

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% D'après Antilles, juin 2016, exercice 2
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\exercice{4}\label{exo:fonctions}

\medskip

On s'intéresse à une modélisation de la propagation de l'épidémie de la grippe en France
durant l'hiver 2014 --- 2015.

Les relevés statistiques, fournis par le réseau Sentinelle, du nombre de cas pour \np{100000}
habitants sur la période du 29 décembre 2014 au 1\up{er} mars 2015 ont permis de mettre en
évidence une courbe de tendance, à l'aide d'un tableur.

Soit $f$ la fonction définie, pour tout $x \in  [2~;~10]$, par 

\[f(x) = - 30x^2 + 360x - 360.\]

On admet que $f(x)$ modélise le nombre de malades déclarés pour \np{100000} habitants au bout
de $x$ semaines écoulées depuis le début de l'épidémie. On note $C$ sa courbe représentative
dans le plan muni d'un repère orthogonal.

\bigskip

\textbf{Partie A}

\medskip

À partir du graphique de l'annexe \ref{annexe:fonctions} (page \pageref{annexe:fonctions}), répondre aux questions suivantes :

\medskip

\begin{enumerate}
\item Selon ce modèle, au bout de combien de semaines le pic de l'épidémie a-t-il été
atteint ?
\item Déterminer le nombre de semaines pendant lesquelles le nombre de malades a été
supérieur ou égal à $600$. On laissera les traits de justification apparents sur le
graphique de l'annexe \ref{annexe:fonctions} (page \pageref{annexe:fonctions}), à rendre avec la copie.
\end{enumerate}

\bigskip

\textbf{Partie B}

\medskip

\begin{enumerate}
\item 
	\begin{enumerate}
		\item Calculer $f'(x)$, où $f'$ désigne la fonction dérivée de $f$ sur l'intervalle [2~;~10] puis
résoudre l'inéquation $f' (x) \geqslant 0$ sur cet intervalle.
		\item En déduire le tableau de variations de $f$ sur l'intervalle [2~;~10].
	\end{enumerate}		
\item On admet que le réel $f'(x)$ représente la vitesse de propagation de l'épidémie au
bout de $x$ semaines.

La grippe se propage-t-elle plus vite au bout de $3$ semaines ou de $4$~semaines ?

Justifier la réponse.
\end{enumerate}

\newpage
\appendix
\newgeometry{top=1.2cm,bottom=1.2cm}
\thispagestyle{empty}

\begin{center}
\textbf{Annexes à rendre avec la copie}
\end{center}

\annexe{exo:evolution}
\label{annexe:evolution}

\begin{center}
\psset{xunit=0.8cm,yunit=0.08cm}
\begin{pspicture}(-1,-5)(12,150)
\multido{\n=0+1}{13}{\psline[linewidth=0.2pt,linecolor=cyan](\n,0)(\n,140)}
\multido{\n=0+10}{15}{\psline[linewidth=0.2pt,linecolor=cyan](0,\n)(12,\n)}
\psaxes[linewidth=1.25pt,Dy=10]{->}(0,0)(0,0)(12,150)
\psdots(0,100)(1,106.8)(2,106.8)(3,109.9)(4,112.7)(5,112.6)(6,120.3)(7,124.9)(8,126)(9,122.7)(10,122.9)
\uput[u](8,0){Nombre d'immatriculations (en milliers)}
\uput[r](0,145){Indice (base 100 en 2001)}
\end{pspicture}
\end{center}

\vfill

\annexe{exo:fonctions}
\label{annexe:fonctions}

\begin{center}
\psset{xunit=0.9cm,yunit=0.0175cm}
\begin{pspicture}(-1,-100)(12,620)
\multido{\n=0+1}{13}{\psline[linewidth=0.3pt,linecolor=orange](\n,0)(\n,600)}
\multido{\n=0+50}{13}{\psline[linewidth=0.3pt,linecolor=orange](0,\n)(12,\n)}
\psaxes[linewidth=1.25pt,Oy=200,Dy=50](0,0)(0,0)(12,600)[Nombre de semaines,100][Nombre de malades déclarés pour \np{100000} habitants,30]
\psplot[plotpoints=4000,linewidth=1.25pt,linecolor=blue]{2}{10}{360 x mul 560 sub x dup mul 30 mul sub}
\uput[ur](8,400){\blue $C$}
\end{pspicture}
\end{center}

\end{document}
