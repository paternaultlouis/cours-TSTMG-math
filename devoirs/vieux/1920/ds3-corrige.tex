%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Copyright 2020 Louis Paternault --- http://ababsurdo.fr
%
% Publié sous licence Creative Commons Attribution-ShareAlike 4.0 International (CC BY-SA 4.0)
% http://creativecommons.org/licenses/by-sa/4.0/deed.fr
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% Pour compiler :
%$ lualatex $basename

\documentclass[12pt]{article}

\usepackage{1920-pablo}
\usepackage{1920-pablo-devoir}
\usepackage[a5paper, margin=0.7cm]{geometry}

\title{Dérivation de polynômes}
\date{Corrigé}
\classe{T\up{ale}STMG}

\setlength\parindent{0mm}
\pagestyle{empty}

\begin{document}
\dsnum{DS \no{3} --- A}
\maketitle

\begin{exercice}
% D'après Baccalauréat STMG Métropole --- 16 juin 2017

\begin{em}
Une entreprise produit et vend un tissu en coton de forme rectangulaire de 1 mètre de large; on
note $x$ sa longueur exprimée en kilomètre, $x$ étant un nombre compris entre 0 et 10.

Le coût total de production en euro de ce tissu est donné, en fonction de $x$, par :

\[C(x) = 15x^3 - 120x^2 + 350x + \numprint{1000}.\]\index{fonction polynôme}

La courbe de la fonction $C $ est représentée sur le graphique ci-dessous.
\end{em}

\begin{center}
    \begin{tikzpicture}[ultra thick, scale=.9]
    \begin{axis}%
        [
            grid=both,
            xscale=1.5,
            yscale=1.2,
            xtick={0,1,...,10},
            %minor x tick num=1,
            %xticklabels={0,5,...,50},
            xmin=0,
            xmax=10,
            ytick={0, 1000, ..., 8000},
	    minor y tick num=1,
            %yticklabels={0, 1000, ..., 8000},
            ymin=0,
            ymax=8000,
            axis y line=left,
            axis x line=middle,
            %samples=100,
            domain=0:10,
            xlabel={Longueur (en km)},
            ylabel={Coût total de production (en euro)},
    %x label style={anchor=north east},
    %y label style={anchor=south east},
            y tick label style={/pgf/number format/.cd,%
	    scaled y ticks = false,
	    set decimal separator={,},
	    set thousands separator={ },
	    fixed
	    }
        ]
        \addplot[ultra thick, blue](x,{15*\x*\x*\x-120*\x*\x+350*\x+1000});
        \draw[dashed, ultra thick] (axis cs:6,0) -- ++(axis cs:0, 2020) -- ++(axis cs:-6, 0);
        \draw[dashed, ultra thick] (axis cs:9.07,0) -- ++(axis cs:0, 5500) -- ++(axis cs:-9.07, 0);
    \end{axis}
    \end{tikzpicture}
\end{center}

\textbf{Partie A : Étude du coût total}

\begin{enumerate}
\item \emph{ Déterminer le montant des coûts fixes.}

Les coûts fixes correspondent au coût de fabrication de zéro kilomètres de tissu, soit \numprint{1000}€.
\item 
	\begin{enumerate}
		\item \emph{Déterminer, par lecture graphique, le montant du coût total lorsque l'entreprise produit \numprint[km]{6}  de tissu.\index{lecture graphique}}

		Par lecture graphique, on trouve environ \numprint{2000}€.
		\item \emph{Déterminer par un calcul sa valeur exacte.}

Le coût de production de \numprint[km]{6} de tissu est $C(x)=15\times6^3-120\times6^2+350\times6+1000=2020$ €.
	\end{enumerate}
		\item \emph{Déterminer graphiquement la longueur, arrondie au kilomètre, de tissu produit lorsque le coût total s'élève à \numprint[€]{5500}.}

		Graphiquement, on lit que cette longueur doit être environ \numprint[km]{9}.
	\end{enumerate}

\textbf{Partie B : Étude du bénéfice}

\begin{em}
Le cours du marché offre un prix de 530\,€  le kilomètre de tissu fabriqué par l'entreprise.

Pour tout $x \in [0~;~10]$, on note $R(x)$ la recette et $B(x)$ le bénéfice générés par la production et la vente de $x$ kilomètres de tissu par l'entreprise.
\end{em}

\begin{enumerate}
\item \emph{Exprimer $R(x)$ en fonction de $x$.}

Chaque kilomètre de tissu fabriqué est vendu \numprint{530}€, donc $R(x)=530x$.
\item \emph{Montrer que pour tout $x\in [0;10]$ : $B(x) = -15x^3 + 120x^2 + 180x - \numprint{1 000}$.}

Le bénéfice est égal aux recettes moins les coûts, soit :
\begin{align*}
B(x)
&= R(x)-C(x)\\
&= 530x-\left(15x^3-120x^2+350x+1000\right)\\
&= 530x-15x^3+120x^2-350x-1000\\
&= -15x^3+120x^2+180x-1000\\
\end{align*}
\item \emph{Déterminer $B'(x)$ pour $x \in [0~;~10]$ où $B'$ désigne la fonction dérivée de $B$.\index{dérivée}}

On a :
\begin{align*}
B(x)
&= -15x^3+120x^2+180x-1000\\
B'(x)&=-15\times3x^2+120\times2x+180\\
&=-45x^2+240x+180
\end{align*}
\item \emph{Étudier le signe de $B'(x)$ et en déduire les variations de la fonction $B$ sur $ [0~;~10]$.}

La fonction dérivée $B'$ est un polynôme du second degré, avec $a=-45$, $b=240$, $c=180$. Le discriminant est $\Delta=b^2-4ac=240^2-4\times\left(-45\right)\times180=90000$.

Ce discriminant est positif, donc il y a deux racines :

\[
x_1=\frac{-b-\sqrt{\Delta}}{2a}=\frac{-240-\sqrt{90000}}{2\times(-45)}=6
   \]
   \[
x_2=\frac{-b+\sqrt{\Delta}}{2a}=\frac{-240+\sqrt{90000}}{2\times(-45)}=-2/3
\]

\begin{center}
  \begin{tikzpicture}[scale=.9]
      \tkzTabInit[lgt=4,espcl=2]
      {$x$ /1,
      {Signe de $B'(x)$} /1,
      {Variations de $B$} /1
      }
      {{-2/3}, 6, 10}%
      \tkzTabLine{,+ , z ,-}
      \tkzTabVar{-/, +/, -/}
    \end{tikzpicture}
\end{center}
\item 
	\begin{enumerate}
		\item \emph{ Pour quelle longueur de tissu produit et vendu l'entreprise réalise-t-elle un bénéfice maximal ?}

		Nous voyons sur le tableau de variations que le maximum de $B$ est atteint pour $x=6$. Il faut donc vendre \numprint[km]{6} de tissu pour que le bénéfice soit maximal.
		\item \emph{Donner alors la valeur de ce bénéfice maximal.}

		Ce bénéfice est alors $B(6)=-15\times6^3+120\times6^2+180\times6-1000=1160$, soit $1160$€.
	\end{enumerate}

\item 
  \begin{enumerate}
    \item \emph{Calculer le nombre dérivé de $B$ en 5.}

    $B'(5)=-45\times5^2+240\times5+180=255$.
    \item \emph{Sur le graphique suivant, tracer la tangente à $B$ au point d'abscisse 5 dans le repère.}

    On part du point de la courbe d'abscisses 5. À partir de ce point, on se déplace d'une unité vers la droite, et de 255 unités vers le haut (car $B'(5)=255$). On trace la droite qui passe par ces deux points.
  \end{enumerate}
\end{enumerate}
\begin{center}
    \begin{tikzpicture}[ultra thick]
    \begin{axis}%
        [
            grid=both,
            xscale=1.5,
            yscale=1.2,
            xtick={0,1,...,10},
            %minor x tick num=1,
            %xticklabels={0,5,...,50},
            xmin=0,
            xmax=10,
            ytick={-2000, -1500, ..., 2000},
            %minor y tick num=4,
            %yticklabels={-2000, -1500, ..., 2000},
            ymin=-2000,
            ymax=2000,
            axis y line=left,
            axis x line=middle,
            %samples=100,
            domain=0:10,
            xlabel={Longueur (en km)},
            ylabel={Bénéfice de la production (en euro)},
    x label style={anchor=north east},
    %y label style={anchor=south east},
            y tick label style={/pgf/number format/.cd,%
	    scaled y ticks = false,
	    set decimal separator={,},
	    set thousands separator={ },
	    fixed
	    }
        ]
        \addplot[ultra thick, blue](x,{-15*\x*\x*\x+120*\x*\x+180*\x-1000});
      \draw (axis cs:5,1025) node{$\times$};
      \draw (axis cs:6,{1025+255}) node{$\times$};
      \addplot[ultra thick, dashed](x, {255*(\x-5)+1025});
    \end{axis}
    \end{tikzpicture}
\end{center}

\end{exercice}

\newpage
\dsnum{DS \no{3} --- B}
\maketitle

\setcounter{exercice}{0}
\begin{exercice}
% D'après le Baccalauréat STMG Nouvelle-Calédonie --- novembre 2019

\begin{em}
Une entreprise fabrique et vend un produit désinfectant liquide. Chaque jour, elle fabrique $x$ hectolitres de désinfectant avec $x$ compris entre $0$ et $12$. On considère que l'entreprise vend toute sa production.

Le coût de fabrication, en dizaine d'euros, de $x$ hectolitres de ce produit est modélisé par la fonction $C$ définie sur l'intervalle $[0~;~12]$.

Le chiffre d'affaires pour la vente de $x$ hectolitres de produit est $R(x)$, exprimé en dizaines d'euros.

Dans un repère orthogonal du plan, on a tracé les représentations graphiques des fonctions $C$ et $R$.
\end{em}

\begin{center}
  \begin{tikzpicture}[ultra thick, scale=.8]
    \begin{axis}%
      [
        grid=both,
        xscale=1.2,
        %yscale=1.9,
        xtick={0,1,...,12},
            %minor x tick num=1,
            %xticklabels={0,5,...,50},
        xmin=0,
        xmax=12,
        ytick={0, 200, ..., 2200},
        %minor y tick num=1,
            %yticklabels={0, 1000, ..., 8000},
        ymin=0,
        ymax=2200,
        axis y line=left,
        axis x line=bottom,
            %samples=100,
        domain=0:12,
        xlabel={Nombre d'hectolitres par jour},
        ylabel={Montant par jour (en dizaine d'euros)},
    %x label style={anchor=north east},
        y label style={anchor=south},
        y tick label style={/pgf/number format/.cd,%
          scaled y ticks = false,
          set decimal separator={,},
          set thousands separator={ },
          fixed
        }
      ]
      \addplot[ultra thick, blue](x,{2*\x^3 -15*\x^2+66*\x+50});
      \addplot[ultra thick, black](x,{150*\x});
      \draw (axis cs:5,200) node[anchor=north west]{Coût de fabrication};
      \draw (axis cs:8,1200) node[anchor=south east]{Chiffre d'affaires};
      \draw[ultra thick, dashed] (axis cs:0,202) -- (axis cs:4, 202);
      \draw[ultra thick, dashed] (axis cs:4,0) -- (axis cs:4, 600);
      \draw[ultra thick, dashed] (axis cs:0,600) -- (axis cs:4, 600);
      \draw[ultra thick, dashed] (axis cs:0,600) -- (axis cs:4, 600);
      \draw[ultra thick, dashed] (axis cs:.55,81.88) node{$\bullet$} -- (axis cs:.55, 0);
      \draw[ultra thick, dashed] (axis cs:11.09,1662.8) node{$\bullet$} -- (axis cs:11.09, 0);
    \end{axis}
  \end{tikzpicture}
\end{center}

\begin{enumerate}
\item \emph{On considère la production d'une journée. Par lecture graphique:}
	\begin{enumerate}
		\item \emph{Déterminer le chiffre d'affaires réalisé pour la vente de 4 hectolitres.}

		Ce chiffre d'affaire est environ 600€.
		\item \emph{Déterminer le coût de fabrication de 4 hectolitres.}

		Ce chiffre d'affaire est environ 200€.
		\item \emph{En déduire le bénéfice réalisé pour la vente de 4 hectolitres.}

		Le bénéfice est environ $600-200=400$€.
		\item \emph{Ce bénéfice est-il maximal pour la production et la vente de 4 hectolitres ?  Justifier.}

		Ce bénéfice n'est pas maximal. En effet, le bénéfice correspond à la différence (en ordonnée) entre la droite et la courbe. Or pour une valeur de $x=8$ par exemple, cette différence est plus grande, donc le bénéfice est plus grand.
	\end{enumerate}
\item \emph{Par lecture graphique, donner sous forme d'intervalle, le nombre d'hectolitres que doit produire l'entreprise pour réaliser des profits, c'est-à-dire un bénéfice strictement positif.}

Entre $x=0,5$ et $x=11$ (environ), la droite du chiffre d'affaires est au dessus de la courbe des coûts de fabrication, donc le bénéfice sera positif entre ces abscisses, c'est-à-dire pour $x\in\left]0,5;11\right[$.
\item \emph{La représentation graphique de la fonction R est une droite qui passe par l'origine du repère et par le point A de coordonnées $(4~;~600)$.}

\emph{Déterminer l'expression de $R(x)$.}

Puisque la droite passe par l'origine, c'est une fonction linéaire d'équation $y=ax$, où $a$ est le coefficient directeur. Ce coefficient est égal à $\frac{600}{4}=150$, donc $R(x)=150x$.
 \item \emph{On note $B$ la fonction qui modélise le bénéfice de l'entreprise en fonction du nombre d'hectolitres de désinfectant vendus. Pour $x$ appartenant à l'intervalle [0~;~12] , on a : }

\[B(x) = -2x^3 + 15x^2 + 84x - 50.\]

	\begin{enumerate}
		\item \emph{On note $B'$ la fonction dérivée de la fonction $B$. Calculer $B'(x)$. }

		On a :
		\begin{align*}
		B(x)&=-2x^3+15x^2+84x-50\\
		B'(x)&=-2\times3x^2+15\times2x+84\\
		&=-6x^2+30x+84\\
		\end{align*}
		\item \emph{Résoudre l'équation $-6x^2 +30x +84 = 0$.}

		C'est  un trinôme du second degré, de discriminant $\Delta=b^2-4ac=30^2-4\times(-6)\times84=2916$. Ce discriminant est positif, donc il y a deux racines :

\[
x_1=\frac{-b-\sqrt{\Delta}}{2a}=\frac{-30-\sqrt{2916}}{2\times(-6)}=7
   \]
   \[
x_2=\frac{-b+\sqrt{\Delta}}{2a}=\frac{-30+\sqrt{2916}}{2\times(-6)}=-2
\]

		\item \emph{Recopier et compléter le tableau de variations ci-dessous:}

		Le trinôme est du signe de $a=-6$ (négatif) à l'extérieur des racines (c'est-à-dire avant -2, et après 7) et du signe opposé (positif) à l'intérieur. Donc le tableau est le suivant.
		
\begin{center}
  \begin{tikzpicture}[scale=.9]
      \tkzTabInit[lgt=4,espcl=2]
      {$x$ /1,
      {Signe de $B'(x)$} /1,
      {Variations de $B$} /1
      }
      {0, 7, 12}%
      \tkzTabLine{,$+$ , z ,$-$}
      \tkzTabVar{-/, +/, -/}
    \end{tikzpicture}
\end{center}
		\item \emph{ Pour quelle quantité de désinfectant produite et vendue le bénéfice est-il maximal ? Quel est alors le bénéfice ?}

		D'après le tableau de variations, le maximum de $B$ est atteint pour $x=7$, et on a $B(7)=-2\times7^3+15\times7^2+84\times7-50=587$. Donc le bénéfice maximal est atteint pour 7 hectolitres de désinfectant produite et vendue, et le bénéfice est alors 587 dizaines d'euros, soit \numprint{5870}~€.
\end{enumerate}
\item 
  \begin{enumerate}
    \item \emph{Calculer le nombre dérivé de $B$ en 5.}

    Le nombre dérivé de $B$ en 5 est $B'(5)=-6\times5^2+30\times5+84=84$.
    \item \emph{Sur le graphique suivant, tracer la tangente à $B$ au point d'abscisse 5 dans le repère.}
    On part du point de la courbe d'abscisses 5. À partir de ce point, on se déplace d'une unité vers la droite, et de 84 unités vers le haut (car $B'(5)=84$). On trace la droite qui passe par ces deux points.
  \end{enumerate}
\end{enumerate}
\begin{center}
  \begin{tikzpicture}[ultra thick, scale=.9]
    \begin{axis}%
      [
        grid=both,
        xscale=1.2,
        %yscale=1.9,
        xtick={0,1,...,12},
            %minor x tick num=1,
            %xticklabels={0,5,...,50},
        xmin=0,
        xmax=12,
        ytick={-500, 0, ..., 1000},
        minor y tick num=4,
            %yticklabels={0, 1000, ..., 8000},
        ymin=-500,
        ymax=1000,
        axis y line=left,
            axis x line=middle,
            %samples=100,
        domain=0:12,
        xlabel={Nombre d'hectolitres par jour},
        ylabel={Bénéfices par jour (en dizaine d'euros)},
    %x label style={anchor=north east},
        y label style={anchor=south},
        y tick label style={/pgf/number format/.cd,%
          scaled y ticks = false,
          set decimal separator={,},
          set thousands separator={ },
          fixed
        }
      ]
      \addplot[ultra thick, blue](x,{-2*\x^3 +15*\x^2+84*\x-50});
      \draw (axis cs:5,495) node{$\times$};
      \draw (axis cs:6,{495+84}) node{$\times$};
      \addplot[ultra thick, dashed](x, {84*(\x-5)+495});
    \end{axis}
  \end{tikzpicture}
\end{center}
\end{exercice}

\end{document}
