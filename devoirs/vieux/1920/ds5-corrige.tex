%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Copyright 2020 Louis Paternault --- http://ababsurdo.fr
%
% Publié sous licence Creative Commons Attribution-ShareAlike 4.0 International (CC BY-SA 4.0)
% http://creativecommons.org/licenses/by-sa/4.0/deed.fr
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% Pour compiler :
%$ lualatex $basename

\documentclass[12pt]{article}

\usepackage{1920-pablo}
\usepackage{1920-pablo-devoir}
\usepackage[a5paper, margin=.5cm]{geometry}

\usepackage{numprint}

\title{Dérivaton de fonctions rationnelles}
\date{16/03/2020}
\classe{T\up{ale}STMG}
\dsnum{DS \no{5}}

\setlength\parindent{0mm}
\pagestyle{empty}

\begin{document}
\maketitle

\begin{exercice}
% D'après le baccalauréat STMG Centres étrangers, 13 juin 2019.
  \begin{em}
    Une entreprise fabrique un engrais biologique liquide.

    Chaque jour, le volume d'engrais liquide fabriqué est compris entre \numprint[m^3]{5}  et \numprint[m^3]{60}.

    Le coût moyen quotidien de production (exprimé en centaine d'euros) de cet engrais est modélisé par
    la fonction $f$ définie sur l'intervalle [5~;~60] par :

    \[f(x) = x - 15 + \dfrac{400}{x}\]

    où $x$ est le volume quotidien d'engrais fabriqué, exprimé en m$^3$.
    La représentation graphique $\mathcal{C}_f$ de la fonction $f$ est donnée dans le repère de la page ci-contre.
  \end{em}

  \textbf{PARTIE A}

  \begin{enumerate}
    \item \emph{Quel est le coût moyen quotidien pour la production de \numprint[m^3]{50} d'engrais ?}

      Le coût moyen est $f(50)=50-15+\frac{400}{50}=43$, soit \numprint{4300} euros.

      On pouvait aussi lire sur le graphique que l'image de 50 est (environ) 43.
    \item \emph{Quels volumes d'engrais faut-il fabriquer pour avoir un coût moyen quotidien de production inférieur ou égal à \numprint{3 500}~\euro ?}

      \begin{description}
        \item[Méthode 1 (graphique)] On cherche sur le graphique les abscisses des points de la courbe situés \emph{au dessous} de l'ordonnée 35. Cela donne : $x\in\intervallef{10; 40}$.
        \item[Méthode 2 (calcul)] Résolvons l'inéquation $f(x)\geq35$.
          \begin{align*}
            f(x)&\geq35\\
            x-15+\frac{400}{x}&\geq35\\
            x-15-35+\frac{400}{x}&\geq0\\
            x-50+\frac{400}{x}&\geq0\\
            \frac{x\times x}{x}-\frac{50\times x}{x}+\frac{400}{x}&\geq0\\
            \frac{x^2-50x+400}{x}&\geq0\\
          \end{align*}
          Étudions le signe du numérateur $x^2-50x+400$ : c'est un trinôme de discriminant $\Delta=\left( -50 \right)^2-4\times1\times400=900$. Il est positif, donc il y a deux racines :
          \begin{enumerate}[$x_1$]
            \item $=\frac{-(-50)-\sqrt{900}}{2\times1}=10$
            \item $=\frac{-(-50)+\sqrt{900}}{2\times1}=40$
          \end{enumerate}
          Le tableau de signes est donc (la troisième ligne est obtenue en multipliant les signes des deux premières) :
          \begin{center}
            \begin{tikzpicture}[scale=.9]
              \tkzTabInit[lgt=4,espcl=2]
              {$x$ /1,
                {$x^2-50x+400$} /1,
                {$x$} /1,
                {$\frac{x^2-50x+400}{x}$} /1
              }
              {5, 10, 40 ,60}%
              \tkzTabLine{,+ , z ,-, z, +}
              \tkzTabLine{,+ ,  t,+, t, +}
              \tkzTabLine{,+ , z ,-, z, +}
            \end{tikzpicture}
          \end{center}
          Donc les solutions sont $x\in\intervallef{10;40}$.
      \end{description}
  \end{enumerate}

  \textbf{PARTIE B}

  \begin{em}
    On admet que la fonction $f$ est dérivable sur l'intervalle [5~;~60]. On note $f'$ sa fonction dérivée.
  \end{em}
  \begin{enumerate}
    \item \emph{Montrer que, pour tout $x$ appartenant à l'intervalle [5~;~60] :}
      \[f'(x) =\dfrac{x^2 - 400}{x^2}\]
      On peut dériver séparément $x-15$ d'une part, et $\frac{400}{x}$ d'autre part : la dérivée de la fonction $x\mapsto x-15$ est $x\mapsto 1$, celle de la fonction $x\mapsto\frac{400}{x}$ est $x\mapsto-\frac{400}{x^2}$.
      \begin{align*}
        f'(x)&=1-\frac{400}{x^2}\\
        &=\frac{x^2}{x^2}-\frac{400}{x^2}\\
        &=\frac{x^2-400}{x^2}
      \end{align*}
    \item \emph{Étudier le signe de $x^2-400$, pour tout $x$ appartenant à l'intervalle [5~;~60].}
      \begin{description}
        \item[Méthode 1] En factorisant : 
          $x^2-400=x^2-20^2=(x-20)(x+20)$. Donc :
          \begin{center}
            \begin{tikzpicture}[scale=.9]
              \tkzTabInit[lgt=4,espcl=2]
              {$x$ /1,
                {$x-20$} /1,
                {$x+20$} /1,
                {$x^2-400=(x-20)(x+20)$} /1
              }
              {5, 20 ,60}%
              \tkzTabLine{,- , z ,+ }
              \tkzTabLine{,+ ,  t,+}
              \tkzTabLine{,- , z ,+}
            \end{tikzpicture}
          \end{center}
        \item[Méthode 2] On remarque que $x^2-400$ est un trinôme du second degré, avec $a=1$, $b=0$ et $c=-400$. Son discriminant est $\Delta=0^2-4\times1\times(-400)=1600$. Il y a donc deux racines.
          \begin{enumerate}[$x_1$]
            \item $=\frac{-0-\sqrt{1600}}{2\times1}=-20$
            \item $=\frac{-0+\sqrt{1600}}{2\times1}=20$
          \end{enumerate}
          Le tableau de signes du trinôme est donc :
          \begin{center}
            \begin{tikzpicture}[scale=.9]
              \tkzTabInit[lgt=4,espcl=2]
              {$x$ /1,
                {$x^2-400$} /1
              }
              {5, 20 ,60}%
              \tkzTabLine{,- , z ,+}
            \end{tikzpicture}
          \end{center}
      \end{description}
    \item \emph{En déduire les variations de la fonction $f$ sur l'intervalle [5~;~60].}
      \begin{center}
        \begin{tikzpicture}[scale=.9]
          \tkzTabInit[lgt=4,espcl=2]
          {$x$ /1,
            {$x^2-400$} /1,
            {$x^2$}/1,
            {$f'(x)=\frac{x^2-400}{x^2}$}/1,
            {$f$}/2
          }
          {5, 20 ,60}%
          \tkzTabLine{,- , z ,+}
          \tkzTabLine{,+ , t ,+}
          \tkzTabLine{,- , z ,+}
          \tkzTabVar{+/, -/, +/}
        \end{tikzpicture}
      \end{center}
    \item \emph{Pour quel volume d'engrais fabriqué le coût moyen quotidien de production est-il minimal ?
      Quel est ce coût moyen minimal ?
    }

    Nous voyons dans le tableau de variations de la question précédente que le minimum est atteint por $x=20$. Donc le coût moyen quotidien de production est minimal pour \numprint[m^3]{20} d'engrais vendus, et le coût moyen est alors $f(20)=20-15+\frac{400}{20}=25$ centaines d'euros, soit \numprint{2500} euros.
  \end{enumerate}

  \begin{center}
    \begin{tikzpicture}[ultra thick]
      \begin{axis}%
        [
          grid=both,
          xscale=1.5,
          yscale=1.5,
          xtick={0,10,...,60},
          minor x tick num=1,
          xmin=0,
          xmax=65,
          ytick={0, 10, ..., 70},
          minor y tick num=1,
          ymin=0,
          ymax=75,
          axis y line=left,
          axis x line=bottom,
          %samples=100,
          domain=5:60,
          xlabel={Volume quotidien (en $m^3$)},
          ylabel={Coût moyen (en centaires d'euros)},
          %x label style={anchor=north east},
          y label style={
            anchor=east,
            at={(ticklabel* cs:1)}
          }
          %y tick label style={/pgf/number format/.cd,%
          %scaled y ticks = false,
          %set decimal separator={,},
          %set thousands separator={ },
          %fixed
          %}
        ]
        \addplot[smooth, ultra thick, blue](x,{\x-15+400/\x});

        \begin{scope}[thick, dashed]
          \draw (axis cs:50, 0) -- (axis cs:50, {50-15+400/50}) node{$\bullet$} -- (axis cs:0, {50-15+400/50}) node[left]{43};
          \draw (axis cs:0, 35) node[left]{35} -- (axis cs:60,35);
          \draw (axis cs:10, 0) -- (axis cs:10, 35) node{$\bullet$};
          \draw (axis cs:40, 0) -- (axis cs:40, 35) node{$\bullet$};
        \end{scope}
      \end{axis}
    \end{tikzpicture}
  \end{center}

\end{exercice}

\end{document}
