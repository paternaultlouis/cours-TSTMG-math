%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Copyright 2017 Louis Paternault --- http://ababsurdo.fr
%
% Publié sous licence Creative Commons Attribution-ShareAlike 4.0 International (CC BY-SA 4.0)
% http://creativecommons.org/licenses/by-sa/4.0/deed.fr
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% Pour compiler :
%$ lualatex $basename

\documentclass[10pt]{article}

\usepackage{1920-pablo}
\usepackage{1920-pablo-paternault}

\usepackage[a5paper, margin=2cm]{geometry}

\usepackage{framed}

\pagestyle{empty}
\setlength{\parindent}{0pt}

\renewcommand{\thesubsection}{\arabic{subsection}.}

\pgfmathdeclarefunction{gauss}{3}{%
  \pgfmathparse{1/(#3*sqrt(2*pi))*exp(-((#1-#2)^2)/(2*#3^2))}%
}

\begin{document}

\begin{center}
\textsc{\large Calcul de termes d'une suite à la calculatrice}

\end{center}
\hrule
~

Toutes les manipulations se font en allant dans le menu « Statistiques » (\texttt{STAT}) :
\touche{MENU}, $\vcenter{\hbox{\includegraphics[height=1.5em]{casio-menu-stat}}}$.

\subsection{Calcul de $P\left(a\leq X\leq b\right)$}
\begin{exercice}
On considère la variable aléatoire $X$ suivant la loi normale d'espérance $\mu=250$ et d'écart-type $\sigma=35$. Calculer $P\left(200\leq X\leq240\right)$.
\end{exercice}

\begin{enumerate}
\item Aller dans le menu \touche{DIST} (touche \touche{F2}), puis \touche{NORM} (touche \touche{F1}), puis \touche{Ncd} (touche \touche{F2}).
\item L'écran présente alors un menu, à remplir comme suit :

\begin{center}
\begin{minipage}{0.5\textwidth}
\begin{framed}
\texttt{D.C. Normale}\\
\texttt{DATA      : Variable}\\
\texttt{Lower     : 200}\\
\texttt{Upper     : 240}\\
$\mathtt{\sigma}$\texttt{         : 35}\\
$\mathtt{\mu}$\texttt{         : 250}\\
\texttt{Save Res: None}
\end{framed}
\end{minipage}
\end{center}

\item Tout en bas de l'écran, sélectionner la ligne \texttt{Exécuter}.
\begin{enumerate}
\item Appuyer sur \touche{CALC} (touche \touche{F1}). Combien vaut $P\left(200\leq X\leq240\right)$ ?
\item
Revenir à l'écran précédent (touche \touche{EXIT}), puis appuyer sur \touche{DRAW} (touche \touche{F6}).

\begin{multicols}{2}
 La calculatrice donne alors une représentation graphique de la probabilité calculée. Compléter ci-contre le graphique affiché. Placer les abscisses 200, 240, 250 sur le graphique.

\columnbreak

\begin{center}
  \begin{tikzpicture}[scale=1]
    \begin{axis}[
        no markers,
        domain=0:6,
        samples=100,
        ymin=0,
        axis lines*=left,
        every axis y label/.style={at=(current axis.above origin),anchor=south},
        every axis x label/.style={at=(current axis.right of origin),anchor=west},
        height=3.3cm,
        width=6cm,
        xtick=\empty,
        ytick=\empty,
        enlargelimits=false,
        clip=false,
        axis on top,
        grid = major,
        hide y axis
      ]

      \pgfmathsetmacro\valueC{gauss(1,1,1)}

      \draw [very thick, red]  (axis cs:3,0) -- (axis cs:3,\valueC);
      \draw [very thick, black] (axis cs:0, 0) -- (axis cs:6, 0);
      \addplot [very thick,black] {gauss(x, 3, 1)};
    \end{axis}
  \end{tikzpicture}
\end{center}
\end{multicols}
\end{enumerate}
\end{enumerate}

\pagebreak

\subsection{Calcul de $P\left(X\leq b\right)$}
\begin{exercice}
On considère la variable aléatoire $X$ suivant la loi normale d'espérance $\mu=120$ et d'écart-type $\sigma=50$. Calculer $P\left(X\leq150\right)$.
\end{exercice}

La calculatrice ne permet pas de calculer une telle probabilité (sans borne inférieure). Du coup, nous allons « tricher » : au lieu de calculer $P\left(X\leq150\right)$, nous allons calculer $P\left(-\infty\leq X\leq150\right)$. Enfin, puisque la calculatrice ne permet pas de rentrer $-\infty$ comme valeur, nous allons rentrer une valeur très petite à la place : $-9999999999$ (le nombre de 9 n'a pas vraiment d'importance, tant qu'il est plutôt grand).

\begin{enumerate}
\item Suivre les instructions de la première partie pour calculer la probabilité voulue (en mettant $-9999999$ comme borne inférieure (\texttt{Lower}).
\item
\begin{multicols}{2}
Afficher la représentation graphique donnée par la calculatrice, puis la recopier en complétant le graphique suivant, en grisant la zone adéquate, et en plaçant correctement les abscisses 120 et 150.

\columnbreak

\begin{center}
  \begin{tikzpicture}[scale=1]
    \begin{axis}[
        no markers,
        domain=0:6,
        samples=100,
        ymin=0,
        axis lines*=left,
        every axis y label/.style={at=(current axis.above origin),anchor=south},
        every axis x label/.style={at=(current axis.right of origin),anchor=west},
        height=3.3cm,
        width=6cm,
        xtick=\empty,
        ytick=\empty,
        enlargelimits=false,
        clip=false,
        axis on top,
        grid = major,
        hide y axis
      ]

      \pgfmathsetmacro\valueC{gauss(1,1,1)}

      \draw [very thick, red]  (axis cs:3,0) -- (axis cs:3,\valueC);
      \draw [very thick, black] (axis cs:0, 0) -- (axis cs:6, 0);
      \addplot [very thick,black] {gauss(x, 3, 1)};
    \end{axis}
  \end{tikzpicture}
\end{center}
\end{multicols}
\end{enumerate}

\subsection{Calcul de $P\left(a\leq X\right)$}

\begin{exercice}
On considère la variable aléatoire $X$ suivant la loi normale d'espérance $\mu=2$ et d'écart-type $\sigma=0,5$. Calculer $P\left(1\leq X\right)$.
\end{exercice}

On utilise ici la même abscisse que précédemment : au lieu de calculer $P\left(1\leq X\right)$, on calcule $P\left(1\leq X\leq+\infty\right)$.

\begin{enumerate}
\item Calculer la probabilité $P\left(1\leq X\right)$.
\item
\begin{multicols}{2}
Afficher la représentation graphique donnée par la calculatrice, puis la recopier en complétant le graphique suivant, en grisant la zone adéquate, et en plaçant correctement les abscisses 120 et 150.

\columnbreak

\begin{center}
  \begin{tikzpicture}[scale=1]
    \begin{axis}[
        no markers,
        domain=0:6,
        samples=100,
        ymin=0,
        axis lines*=left,
        every axis y label/.style={at=(current axis.above origin),anchor=south},
        every axis x label/.style={at=(current axis.right of origin),anchor=west},
        height=3.3cm,
        width=6cm,
        xtick=\empty,
        ytick=\empty,
        enlargelimits=false,
        clip=false,
        axis on top,
        grid = major,
        hide y axis
      ]

      \pgfmathsetmacro\valueC{gauss(1,1,1)}

      \draw [very thick, red]  (axis cs:3,0) -- (axis cs:3,\valueC);
      \draw [very thick, black] (axis cs:0, 0) -- (axis cs:6, 0);
      \addplot [very thick,black] {gauss(x, 3, 1)};
    \end{axis}
  \end{tikzpicture}
\end{center}
\end{multicols}
\end{enumerate}

\end{document}
