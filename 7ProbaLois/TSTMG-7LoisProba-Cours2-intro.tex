%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Copyright 2020 Louis Paternault --- http://ababsurdo.fr
%
% Publié sous licence Creative Commons Attribution-ShareAlike 4.0 International (CC BY-SA 4.0)
% http://creativecommons.org/licenses/by-sa/4.0/deed.fr
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% Pour compiler :
%$ lualatex $basename
%$ lualatex $basename

\documentclass[12pt]{article}

\usepackage{1920-pablo}
\usepackage{1920-pablo-paternault}
\usepackage[a5paper, bottom=1.5cm, left=1.5cm, right=1.5cm]{geometry}

\usepackage{fancyhdr}
\pagestyle{fancy}
\fancyhead[L]{\textsc{Chapitre 7 --- Lois de probabilité}}
\fancyhead[R]{\textsc{Intro 2}}
\fancyfoot[C]{}

\setlength{\parindent}{0pt}

% Point médian
\newcommand\pointmedian{\kern-0.25em\textperiodcentered\kern-0.25em}
\catcode`·=\active
\def·{\pointmedian}

\usepackage{tabularx}
\usepackage{mdframed}

\usepackage{qrcode}

\renewcommand{\blanc}[1]{\textcolor{red}{\large #1}}

\begin{document}

\setcounter{section}{1}
\section*{Introduction à la loi normale}

\begin{em}Cette partie a pour but de vous faire travailler sur la \emph{symétrie} de la loi normale.

  Il n'y a rien à écrire dans votre cahier de cours : prenez une feuille de brouilon et un crayon, et essayer de faire et comprendre cette activité.
\end{em}

On lance 100 fois une pièce, et on obtient 37 fois pile. On se demande si cette pièce est truquée ou non.

On imagine maintenant qu'on lance une \emph{autre} pièce, équilibrée, 100 fois de suite, et qu'on compte le nombre de pile obtenus (que l'on associe à la variable aléatoire $X$). Les probabilités sont résumées dans le graphique suivant (les probabilits pour $X<30$ et $X>70$ ne sont pas affichée car elles sont si proches de 0 que l'on peut les considérer comme nulles).

\begin{center}
  \begin{tikzpicture}[scale=1]
    \begin{axis}[
        xmin=30,
        xmax=70,
        ymin=0,
        xscale=1.5,
        yscale=1,
        ymax=0.085,
        grid style={line width=.2pt, draw=gray!50},
        major grid style={line width=.4pt,draw=black},
        grid=both,
        axis lines=center,
        xlabel={Nombre de pile},
        ylabel={Probabilité},
        xlabel style={at={(axis cs:70,-0.01)},anchor=north east},
        ylabel style={at={(axis cs:25,.05)},rotate=90,anchor=south},
        xtick={30, 40, ..., 70},
        ytick={0, 0.01, ..., 0.085},
        minor x tick num=9,
        minor ytick={0, 0.005, ..., 0.085},
        scaled y ticks = false,
        y tick label style={/pgf/number format/fixed},
      ]
      \addplot[%
        ybar interval,
        fill=blue!30,
        line width=0.4pt,
      ]%
      table {intro_binomiale.data};
    \end{axis}
  \end{tikzpicture}
\end{center}

Le graphique se lit de la manière suivante. Par exemple, puisque la barre d'abscisse 50 monte jusqu'à 0,08, alors $P(X=50)=0,08$ (en d'autres termes, la probabilité d'obtenir exactement 50 pile est de 0,08, soit 8\% environ).

\begin{em}
  Les réponses à cette question se trouvent à la page \pageref{intro:solution}. Essayez de répondre aux questions avant de regarder les solutions.
\end{em}

\begin{enumerate}
  \item Lire graphiquement $P\left(X=40\right)$. En déduire la probabilité d'obtenir exactement 40 fois pile.
  \item Comment pourrait-on faire pour lire graphiquement la probabilité $P\left(51\leq X\leq60\right)$, c'est-à-dire la probabilité d'obtenir entre 51 et 60 fois pile ? Ne pas faire le calcul : donner seulement la méthode.
\end{enumerate}
On admet que $P\left(51\leq X\leq60\right)=0,442$.
\begin{enumerate}[resume]
  \item  Graphiquement, comparer $P\left(51\leq X\leq60\right)$ et $P\left(40\leq X\leq 49\right)$. En déduire $P\left(40\leq X\leq 49\right)$.
  \item Calculer $P\left(40\leq X\leq60\right)$.
  \item \emph{Sans les calculer,} comparer (graphiqement, si nécessaire) $P\left(X<40\right)$ et $P\left(X>60\right)$.
  \item En déduire $P\left(X<40\right)$, et reformuler cette probabilité en utilisant une phrase en français.
  \item Que peut-on affirmer à propos de la pièce qui n'a produit que 37 fois pile en 100 lancers ?
\end{enumerate}

\subsection*{Solutions}
\label{intro:solution}
\begin{enumerate}
  \item On lit $P(X=40)\approx0,011$. Donc la probabilité d'obtenir exactement 40 fois pile est d'environ 0,011 (soit 1,1\%).
  \item On pourrait additionner les probabilités de tous les nombres entre 51 et 60 :
    \begin{align*}
      P(51\leq X\leq60)&=
      P(X=51)+ P(X=52)+ P(X=53)\\
      &+P(X=54)+ P(X=55)+ P(X=56)\\
      &+P(X=57)+ P(X=58)+ P(X=59)\\
      &+P(X=60)
    \end{align*}
  \item On remarque que la courbe est symétrique par rapport à l'abscisse 50, et donc que $P(51\leq X\leq60)=P(40\leq X\leq49)$. Donc puisque 
    $P\left(51\leq X\leq60\right)=0,442$, alors
    $P\left(40\leq X\leq49\right)=0,442$.
  \item On \enquote{découpe} la probabilité recherchée en trois \enquote{morceaux} : entre 40 et 49, exactement 50, puis entre 51 et 60 :
    \begin{align*}
      &P(40\leq X\leq60)\\
      &=
      P(40\leq X\leq49)+
      P(X=50)+
      P(51\leq X\leq60)
      &P(40\leq X\leq60)\\
      &= P(40\leq X\leq49)+ P(X=50)+ P(51\leq X\leq60)\\
      &=0,442+0,08+0,442\\
      &=0,964
    \end{align*}
    Donc la probabilité d'obtenir entre 40 et 60 fois pile est égale à 0,964 (soit 96,4\% environ).
  \item Encore une fois, puisque la courbe est symétrique, on observe que $P(X<40)=P(X>60)$ (en d'autres termes, il est aussi probable d'obtenir moins de 40 fois pile que plus de 60 fois pile).
  \item Sur 100 lancers, le nombre de pile obtenus est compris entre 0 et 100.
    \begin{align*}
      P(0\leq X\leq100) &= 1\\
      P(X<40) + P(40\leq X\leq60) + P(X>60) &= 1\\
      P(X<40)+0,964+P(X>60)&=1\\
      P(X<40)+0,964+P(X<40)&=1\\
      2P(X<40)+0,964&=1\\
      2P(X<40)&=1-0,964\\
      2P(X<40)&=0,036\\
      P(X<40)&=\frac{0,036}{2}\\
      P(X<40)&=0,0168
    \end{align*}
    Donc $P(X<40)=0,0168$, soit environ 1,7\%. La probabilité d'obtenir moins de 40 fois pile est d'environ 1,7\%.
  \item Puisque que la probabilité d'obtenir moins de 40 fois pile est d'environ 1,7\%, et que j'ai obtenu, avec ma pièce, 37 fois seulement pile, je peux affirmer (avec une petite probabilité de me tromper) que ma pièce est truquée.
\end{enumerate}

\end{document}
