%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Copyright 2017-2020 Louis Paternault --- http://ababsurdo.fr
%
% Publié sous licence Creative Commons Attribution-ShareAlike 4.0 International (CC BY-SA 4.0)
% http://creativecommons.org/licenses/by-sa/4.0/deed.fr
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% Pour compiler :
%$ lualatex $basename

\documentclass[11pt]{article}

\usepackage{1920-pablo}
\usepackage{1920-pablo-paternault}
\usepackage[a5paper, bottom=10mm, left=7mm, right=7mm]{geometry}

\usepackage{fancyhdr}
\pagestyle{fancy}
\fancyhead[L]{\textsc{Chapitre 7 --- Lois de probabilité}}
\fancyhead[R]{\textsc{Cours 3}}
\fancyfoot[C]{}

\setlength{\parindent}{0pt}

\pgfmathdeclarefunction{gauss}{3}{%
  \pgfmathparse{1/(#3*sqrt(2*pi))*exp(-((#1-#2)^2)/(2*#3^2))}%
}

\usepackage{tabularx}
\usepackage{mdframed}

\usepackage{qrcode}
\usepackage[color=blackandwhite]{graph35}

\renewcommand{\blanc}[1]{\textcolor{red}{\large #1}}

\begin{document}

\setcounter{section}{2}
\section{Calculatrice}
\begin{em}
  Les instructions présentées ici concernent la calculatrice \textsc{Casio Graph 35}. Pour la \emph{TI-83 premium CE}, voir :\\
  \url{http://math.univ-lyon1.fr/irem/IMG/pdf/170_ti83_Premium_CE.pdf}.
\end{em}

Toutes les manipulations se font en allant dans le menu « Statistiques » : \key{MENU} \menu{STAT}{2}.

\subsection{Calcul de $P\left(a\leq X\leq b\right)$}
\begin{exercice}
  On considère la variable aléatoire $X$ suivant la loi normale d'espérance $\mu=250$ et d'écart-type $\sigma=35$. Calculer $P\left(200\leq X\leq240\right)$.
\end{exercice}

\begin{multicols}{2}
  \begin{enumerate}
    \item Aller dans le menu \function{DIST-b} (touche \key{F2}), puis \function{NORM-b} (touche \key{F1}), puis \function{Ncd} (touche \key{F2}).
    \item L'écran présente alors un menu, à remplir comme ci-contre :
  \end{enumerate}

  \begin{center}
    \begin{minipage}{0.5\textwidth}
      \begin{mdframed}
        \texttt{D.C. Normale}\\
        \texttt{DATA      : Variable}\\
        \texttt{Lower     : 200}\\
        \texttt{Upper     : 240}\\
        $\mathtt{\sigma}$\texttt{         : 35}\\
        $\mathtt{\mu}$\texttt{         : 250}\\
        \texttt{Save Res: None}
      \end{mdframed}
    \end{minipage}
  \end{center}
\end{multicols}

\begin{enumerate}
    \setcounter{enumi}{2}
  \item Tout en bas de l'écran, sélectionner la ligne \texttt{Exécuter}.
    \begin{enumerate}
      \item Appuyer sur \function{CALC} (touche \key{F1}). Combien vaut $P\left(200\leq X\leq240\right)$ ?
        \emph{Solution : environ 0,31.}
      \item
        Revenir à l'écran précédent (touche \function{EXIT}), puis appuyer sur \function{DRAW} (touche \key{F6}).

        \begin{multicols}{2}
          La calculatrice donne alors une représentation graphique de la probabilité calculée. Essayez de placer les abscisses 200, 240, 250 sur le graphique affiché par la calculatrice.

          \columnbreak

          \begin{center}
            \begin{tikzpicture}[scale=1]
              \begin{axis}[
                  no markers,
                  domain=1.5:3.5,
                  samples=100,
                  ymin=0,
                  axis lines*=left,
                  every axis y label/.style={at=(current axis.above origin),anchor=south},
                  every axis x label/.style={at=(current axis.right of origin),anchor=west},
                  height=3cm,
                  width=6cm,
                  xtick=\empty,
                  ytick=\empty,
                  enlargelimits=false,
                  clip=false,
                  axis on top,
                  grid = major,
                  hide y axis
                ]

                \pgfmathsetmacro\valueC{gauss(2.5,2.5,.35)}

                \addplot [preaction={fill=yellow!25}, pattern=north west lines, domain=2:2.4] {gauss(x, 2.5, .35)} \closedcycle;
                \draw [very thick, red]  (axis cs:2.5,0) -- (axis cs:2.5,\valueC);
                \draw [very thick, black] (axis cs:1.5, 0) -- (axis cs:3.5, 0);
                \addplot [very thick,black] {gauss(x, 2.5, .35)};
                \draw (axis cs:2, 0) node[rotate=90, anchor=east]{200};
                \draw (axis cs:2.4, 0) node[rotate=90, anchor=east]{240};
                \draw (axis cs:2.5, 0) node[rotate=90, anchor=east]{250};
              \end{axis}
            \end{tikzpicture}
            \emph{Solution}
          \end{center}
        \end{multicols}
    \end{enumerate}
\end{enumerate}

\pagebreak

\subsection{Calcul de $P\left(X\leq b\right)$}
\begin{exercice}
  On considère la variable aléatoire $X$ suivant la loi normale d'espérance $\mu=120$ et d'écart-type $\sigma=50$. Calculer $P\left(X\leq150\right)$.
\end{exercice}

La calculatrice ne permet pas de calculer une telle probabilité (sans borne inférieure). Du coup, nous allons « tricher » : au lieu de calculer $P\left(X\leq150\right)$, nous allons calculer $P\left(-\infty\leq X\leq150\right)$. Enfin, puisque la calculatrice ne permet pas de rentrer $-\infty$ comme valeur, nous allons rentrer une valeur très petite à la place : $-9999999999$ (le nombre de 9 n'a pas vraiment d'importance, tant qu'il est plutôt grand).

\begin{enumerate}
  \item Suivre les instructions de la première partie pour calculer la probabilité voulue (en mettant $-9999999$ comme borne inférieure (\texttt{Lower})).
    \emph{Solution : Vous devriez obtenir environ 0,73.}
  \item
    \begin{multicols}{2}
      Afficher la représentation graphique donnée par la calculatrice, puis placer sur le graphique les abscisses 120 et 150.

      \columnbreak

      \begin{center}
        \begin{tikzpicture}[scale=1]
          \begin{axis}[
              no markers,
              domain=0:2.5,
              samples=100,
              ymin=0,
              axis lines*=left,
              every axis y label/.style={at=(current axis.above origin),anchor=south},
              every axis x label/.style={at=(current axis.right of origin),anchor=west},
              height=3cm,
              width=6cm,
              xtick=\empty,
              ytick=\empty,
              enlargelimits=false,
              clip=false,
              axis on top,
              grid = major,
              hide y axis
            ]

            \pgfmathsetmacro\valueC{gauss(1.2,1.2,.5)}

            \addplot [preaction={fill=yellow!25}, pattern=north west lines, domain=0:1.5] {gauss(x, 1.2, .5)} \closedcycle;
            \draw [very thick, red]  (axis cs:1.2,0) -- (axis cs:1.2,\valueC);
            \draw [very thick, black] (axis cs:0, 0) -- (axis cs:2.5, 0);
            \addplot [very thick,black] {gauss(x, 1.2, .5)};
            \draw (axis cs:1.5, 0) node[rotate=90, anchor=east]{150};
            \draw (axis cs:1.2, 0) node[rotate=90, anchor=east]{120};
          \end{axis}
        \end{tikzpicture}
        \emph{Solution}
      \end{center}
    \end{multicols}
\end{enumerate}

\subsection{Calcul de $P\left(a\leq X\right)$}

\begin{exercice}
  On considère la variable aléatoire $X$ suivant la loi normale d'espérance $\mu=2$ et d'écart-type $\sigma=0,5$. Calculer $P\left(1\leq X\right)$.
  \emph{Solution : Vous devriez obtenir environ 0,98.}
\end{exercice}

On utilise ici la même abscisse que précédemment : au lieu de calculer $P\left(1\leq X\right)$, on calcule $P\left(1\leq X\leq+\infty\right)$.

\begin{enumerate}
  \item Calculer la probabilité $P\left(1\leq X\right)$.
  \item 
    \begin{multicols}{2}
      Afficher la représentation graphique donnée par la calculatrice, puis la recopier en complétant le graphique suivant, en grisant la zone adéquate, et en plaçant correctement les abscisses 1 et 2.

      \begin{center}
        \begin{tikzpicture}[scale=1]
          \begin{axis}[
              no markers,
              domain=.5:3.5,
              samples=100,
              ymin=0,
              axis lines*=left,
              every axis y label/.style={at=(current axis.above origin),anchor=south},
              every axis x label/.style={at=(current axis.right of origin),anchor=west},
              height=3cm,
              width=6cm,
              xtick=\empty,
              ytick=\empty,
              enlargelimits=false,
              clip=false,
              axis on top,
              grid = major,
              hide y axis
            ]

            \pgfmathsetmacro\valueC{gauss(2,2,.5)}

            \addplot [preaction={fill=yellow!25}, pattern=north west lines, domain=1:3.5] {gauss(x, 2, .5)} \closedcycle;
            \draw [very thick, red]  (axis cs:2,0) -- (axis cs:2,\valueC);
            \draw [very thick, black] (axis cs:0.5, 0) -- (axis cs:3.5, 0);
            \addplot [very thick,black] {gauss(x, 2, .5)};
            \draw (axis cs:2, 0) node[rotate=90, anchor=east]{2};
            \draw (axis cs:1, 0) node[rotate=90, anchor=east]{1};
          \end{axis}
        \end{tikzpicture}

        \emph{Solution}
      \end{center}
    \end{multicols}
\end{enumerate}

\pagebreak
\section*{Exercices corrigés}

\begin{exercice}[Questions diverses]~
  \begin{em}
    Pour chacune de ces questions, il faut calculer une probabilité en utilisant une loi normale, c'est-à-dire :
    \begin{itemize}
      \item identifier la moyenne et l'écart-type ;
      \item identifier s'il faut calculer $P(a\leq X\leq b)$, $P(a\leq X)$ ou $P(X\leq b)$ (et identifier la valeur de $a$ et $b$) ;
      \item faire le calcul à la calculatrice ;
      \item et éventuellement faire un petit calcul ensuite.
    \end{itemize}
  \end{em}

  \begin{enumerate}
    \item\emph{Pondichéry 17 avril 2015}
      Une machine fabrique plusieurs milliers de ces jetons par jour. On désigne par $X$ la variable aléatoire qui, à chaque jeton, associe son diamètre en millimètres.

      On admet que $X$ suit la loi normale d’espérance 20 et d’écart-type 0,015.
      Les jetons sont acceptables si leurs diamètres appartiennent à l’intervalle $\left[19,98; 20,02\right]$.

      La probabilité qu’un jeton pris au hasard dans la production soit acceptable, arrondie à $10^{-3}$, est :
      \begin{center}\begin{tabularx}{\textwidth}{XXXX}
          $\bullet$ 0,818 &
          $\bullet$ $4,84 \times10^{-4}$ &
          $\bullet$ 0,182 &
          $\bullet$ 0
        \end{tabularx}\end{center}
      \begin{em}
        Solution : Les jetons sont acceptables si leurs diamètres appartiennent à l'intervalle $\intervallef{19,98;20,02}$, donc nous cherchons $P\left( 19,98\leq X\leq20,02 \right)\approx0,818$.
      \end{em}
    \item \emph{Centres étrangers 11 juin 2015}
      Un laboratoire pharmaceutique fabrique des gélules contenant une substance S. La masse de substance S, exprimée en milligrammes (mg), contenue dans une gélule est modélisée par une variable aléatoire $X$ suivant la loi normale d’espérance 8,2 et d’écart type 0,05.

      La norme de fabrication impose que la masse de substance S dans une gélule soit comprise entre 8,1 mg et 8,3 mg. La probabilité qu’une gélule soit hors norme après la fabrication est :
      \begin{center}\begin{tabularx}{\textwidth}{XXXX}
          $\bullet$
          0,2
          &
          $\bullet$
          0,05
          &
          $\bullet$
          0,8
          &
          $\bullet$
          0,95
        \end{tabularx}\end{center}
      \begin{em}
        Solution : La gélule est dans les normes si sa masse est dans l'intervalle $\intervallef{8,1;8,3}$, donc la probabilité que la gellule soit dans les normes est $P\left( 8,1\leq X\leq 8,3 \right)\approx0,95$. Donc la probabilité que l agellule \emph{ne soit pas dans les normes} est $1-P\left( 8,1\leq X\leq 8,3 \right)\approx1-0,95\approx0,05$.
      \end{em}
    \item \emph{Nouvelle-Calédonie 19 novembre 2015}
      $X$ est une variable aléatoire qui suit la loi normale d’espérance $\mu=59$ et d’écart type $\sigma=0,2$.
      $p(X<59)$ vaut :
      \begin{center}\begin{tabularx}{\textwidth}{XXXX}
          $\bullet$
          0,5
          &
          $\bullet$
          0,35
          &
          $\bullet$
          0,16
          &
          $\bullet$
          0,96
        \end{tabularx}\end{center}
      \begin{em}Solution :
        On peut résoudre cette question sans calculatrice : nous cherchons la probabilité que $X$ soit inférieure à 59. Or 59 est l'espérance de $X$. Donc, puisque la loi normale est symétrique par rapport à l'espérance, on a $P(X<59)=0$.
      \end{em}
  \end{enumerate}
\end{exercice}

\pagebreak
\section*{Bilan}

Faire les deux exercices suivants, et me les rendre par l'ENT ou par courriel.

\begin{em}
  Avertissement : Je sais que les corrigés sont disponibles en ligne, mais \textsc{par pitié}, ne recopiez pas les corrigés sans les comprendre ! C'est une perte de temps pour vous (car vous n'apprendrez absolument \textsc{rien} en recopiant sans comprendre) comme pour moi (car je ne saurai pas quel est votre niveau réel).
\end{em}

\begin{exercice}[D'apprès le baccalauréat STMG Métropole–La Réunion, 18 juin 2015]
  Une des attractions du parc, une descente de type rafting dans des bouées géantes, attire beaucoup de visiteurs.

  Les normes de sécurité imposent que le bassin d’arrivée cont ienne un volume d’eau compris entre 150 et 170 m\up{3} d’eau. Chaque soir, à la fermeture du parc, l’équipe de maintenance effectue des vérifications et décide, ou non, d’intervenir. Le volume d’eau (exprimé en m\up{3}) contenu dans le bassin, à la fin d’une journée d’exploitation de cette attraction, est modélisé par une variable aléatoire $X$ suivant une loi normale d’espérance $\mu=160$ et d’écart type $\sigma=5$.
  \begin{enumerate}
    \item 
      \begin{enumerate}
        \item Calculer $p(150\leq X\leq 170)$.
        \item En déduire la probabilité que l’équipe de maintenance soit obligée d’intervenir pour respecter les normes de sécurité.
      \end{enumerate}
    \item Quelle est la probabilité que l’équipe de maintenance soit obligée, pour respecter les normes, de rajouter de l’eau dans le bassin à la fin d’une journée d’ouverture?
  \end{enumerate}
\end{exercice}

\pagebreak
\begin{exercice}[D'après le baccalauréat STMG Antilles, 18 juin 2015]
  On rappelle que cette entreprise est composée de \numprint{1200} techniciens et de $800$ ingénieurs.

  On modélise le salaire mensuel, exprimé en euros, d'un technicien de l'entreprise par une
  variable aléatoire $X_T$ suivant une loi normale d'espérance $m_T$ et d'écart type 200.

  On modélise le salaire mensuel, exprimé en euros, d'un ingénieur de l'entreprise par une
  variable aléatoire $X_I$ suivant une loi normale d'espérance $m_I$ et d'écart type 150.

  On donne ci-dessous la représentation graphique des fonctions de densité des variables $X_T$ et $X_I$.

  \begin{center}
    \begin{tikzpicture}[scale=1]
      \begin{axis}[
        %no markers,
          domain=1.4:2.5,
          samples=100,
        %ymin=0,
        %axis lines*=left,
        %every axis y label/.style={at=(current axis.above origin),anchor=south},
        %every axis x label/.style={at=(current axis.right of origin),anchor=west},
          height=6cm,
          width=12cm,
        %xtick=\empty,
          scaled y ticks=false,
          scaled x ticks={real:1000},
          xtick scale label code/.code={},
        %ytick=\empty,
          enlargelimits=true,
          yticklabel style={
            /pgf/number format/fixed,
            /pgf/number format/precision=5
          },
          xticklabels={% C'est moche, mais ça marche…
            1300,
            1400,
            1500,
            1600,
            1700,
            1800,
            1900,
            2000,
            2100,
            2200,
            2300,
            2400,
            2500,
          },
          axis x line=middle,
          axis y line=middle,
          /pgf/number format/use comma,
          grid=both,
          minor y tick num=4,
          grid style={line width=.1pt, draw=gray!10},
          major grid style={line width=.2pt,draw=gray!50}
        ]

      %\draw [very thick, black] (axis cs:1.5, 0) -- (axis cs:3.5, 0);
        \addplot [very thick,black] {gauss(x, 1.8, .2)/1000};
        \addplot [dashed, very thick,black] {gauss(x, 2.2, .15)/1000};
        \legend{$X_T$, $X_I$}
      \end{axis}
    \end{tikzpicture}
  \end{center}

  \begin{enumerate}
    \item Déterminer graphiquement $m_T$ et $m_I$.
    \item Donner une valeur arrondie au centième de $p\left(X_T \leq  \numprint{1600}\right)$.
    \item En déduire une estimation du nombre de techniciens dont le salaire mensuel est
      inférieur ou égal à \numprint{1600}~€ par mois.
  \end{enumerate}
\end{exercice}

\end{document}
