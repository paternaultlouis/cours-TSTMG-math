%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Copyright 2017 Louis Paternault --- http://ababsurdo.fr
%
% Publié sous licence Creative Commons Attribution-ShareAlike 4.0 International (CC BY-SA 4.0)
% http://creativecommons.org/licenses/by-sa/4.0/deed.fr
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% Pour compiler :
%$ lualatex $basename

\documentclass[12pt]{article}

\usepackage{1920-pablo}
\usepackage{1920-pablo-paternault}
\usepackage[a4paper,margin=1cm]{geometry}

\usepackage{tabularx}

\usetikzlibrary{patterns}

\pagestyle{empty}
\setlength{\parindent}{0cm}

\pgfmathdeclarefunction{gauss}{3}{%
  \pgfmathparse{1/(#3*sqrt(2*pi))*exp(-((#1-#2)^2)/(2*#3^2))}%
}

\begin{document}

\section{Loi normale}

Une variable aléatoire de loi binomiale $n$ et $p$, lorsque $n$ est assez grand et que $n\times p$ est supérieur à 5, peut être modélisée par une \emph{loi normale} de même espérance et écart-type.

\begin{definition}[Rappels]
  Étant donné une variable aléatoire $X$ :
  \begin{itemize}
    \item son \blanc{espérance} est la moyenne des valeurs qu'elle peut prendre ;
    \item son \blanc{écart-type} mesure la dispersion des valeurs qu'elle peu prendre (plus il est petit, plus ses valeurs sont proches de son espérance ; plus il est grand, plus ses valeurs peuvent être éloignées de son espérance).
  \end{itemize}
\end{definition}

\begin{propriete}
  La courbe de densité d'une variable aléatoire $X$ suit une loi normale d'espérance $\mu$ (« mu ») et d'écart-type $\sigma$ (« sigma ») est la \blanc{courbe en cloche}.

  \begin{itemize}
    \item Elle est symétrique par rapport à la droite verticale d'équation $x=\mu$.
    \item Pour des réels $k$ et $l$, les probabilités $P\left( X<k \right)$, $P\left( X>k \right)$, $P\left( k<X<l \right)$ peuvent se lire (en théorie) en mesurant l'aire sous la courbe.
  \end{itemize}
\end{propriete}

\begin{tabularx}{\textwidth}{*{3}{>{\centering \arraybackslash}X}}
  \begin{tikzpicture}[scale=1]
    \begin{axis}[
        no markers,
        domain=0:6,
        samples=100,
        ymin=0,
        axis lines*=left,
        every axis y label/.style={at=(current axis.above origin),anchor=south},
        every axis x label/.style={at=(current axis.right of origin),anchor=west},
        height=3.3cm,
        width=6cm,
        xtick=\empty,
        ytick=\empty,
        enlargelimits=false,
        clip=false,
        axis on top,
        grid = major,
        hide y axis
      ]

      \pgfmathsetmacro\valueA{gauss(1,1,1.7)}
      \pgfmathsetmacro\valueC{gauss(1,1,1)}


      \addplot [preaction={fill=yellow!25}, pattern=north west lines, domain=0:2] {gauss(x, 3, 1)} \closedcycle;
      \draw [very thick, red]  (axis cs:2,0) -- (axis cs:2,\valueA);
      \draw [very thick, red, dashed]  (axis cs:3,0) -- (axis cs:3,\valueC);
      \addplot [very thick,black] {gauss(x, 3, 1)};
      \node[below] at (axis cs:3, 0)  {$\mu$};
      \node[below] at (axis cs:2,0) {$k$};
      \draw[-latex] ($(axis cs:1, 0)+(axis cs:-1, .2)$) node[above]{$P\left(X<k\right)$} -- (axis cs:1.6, .07);
    \end{axis}
  \end{tikzpicture}
  &
  \begin{tikzpicture}[scale=1]
    \begin{axis}[
        no markers,
        domain=0:6,
        samples=100,
        ymin=0,
        axis lines*=left,
        every axis y label/.style={at=(current axis.above origin),anchor=south},
        every axis x label/.style={at=(current axis.right of origin),anchor=west},
        height=3.3cm,
        width=6cm,
        xtick=\empty,
        ytick=\empty,
        enlargelimits=false,
        clip=false,
        axis on top,
        grid = major,
        hide y axis
      ]

      \pgfmathsetmacro\valueA{gauss(1,1,1.7)}
      \pgfmathsetmacro\valueC{gauss(1,1,1)}


      \addplot [preaction={fill=yellow!25}, pattern=north west lines, domain=2:6] {gauss(x, 3, 1)} \closedcycle;
      \draw [very thick, red]  (axis cs:2,0) -- (axis cs:2,\valueA);
      \draw [very thick, red, dashed]  (axis cs:3,0) -- (axis cs:3,\valueC);
      \addplot [very thick,black] {gauss(x, 3, 1)};
      \node[below] at (axis cs:3, 0)  {$\mu$};
      \node[below] at (axis cs:2,0) {$k$};
      \draw[-latex] ($(axis cs:4, 0)+(axis cs:1.2, .3)$) node[above]{$P\left(k<X\right)$} -- (axis cs:4, .07);
    \end{axis}
  \end{tikzpicture}
  &
  \begin{tikzpicture}[scale=1]
    \begin{axis}[
        no markers,
        domain=0:6,
        samples=100,
        ymin=0,
        axis lines*=left,
        every axis y label/.style={at=(current axis.above origin),anchor=south},
        every axis x label/.style={at=(current axis.right of origin),anchor=west},
        height=3.3cm,
        width=6cm,
        xtick=\empty,
        ytick=\empty,
        enlargelimits=false,
        clip=false,
        axis on top,
        grid = major,
        hide y axis
      ]

      \pgfmathsetmacro\valueA{gauss(2,3,1)}
      \pgfmathsetmacro\valueB{gauss(4.5,3,1)}
      \pgfmathsetmacro\valueC{gauss(3,3,1)}


      \addplot [preaction={fill=yellow!25}, pattern=north west lines, domain=2:4.5] {gauss(x, 3, 1)} \closedcycle;
      \draw [very thick, red]  (axis cs:2,0) -- (axis cs:2,\valueA);
      \draw [very thick, red]  (axis cs:4.5,0) -- (axis cs:4.5,\valueB);
      \draw [very thick, red, dashed]  (axis cs:3,0) -- (axis cs:3,\valueC);
      \addplot [very thick,black] {gauss(x, 3, 1)};
      \node[below] at (axis cs:3, 0)  {$\mu$};
      \node[below] at (axis cs:2,0) {$k$};
      \node[below] at (axis cs:4.5,0) {$l$};
      \draw[-latex] ($(axis cs:4, 0)+(axis cs:1.5, .3)$) node[above]{$P\left(k<X<l\right)$} -- (axis cs:4, .07);
    \end{axis}
  \end{tikzpicture}
  \\
\end{tabularx}

\begin{propriete}
  Pour tout réel $k$, on a :
  $P\left( X=k \right)=\blanc{0}$
  \hspace{\stretch{1}}
  $P\left( X<k \right)=\blanc{P\left( X\leq k \right)}$
  \hspace{\stretch{1}}
  $P\left( X>k \right)=\blanc{P\left( X\geq k \right)}$
\end{propriete}

\begin{propriete}~
  \begin{center}
    \begin{tabularx}{\textwidth}{*{3}{>{\centering \arraybackslash}X}}
      $P\left( X<k \right)=\blanc{1-P\left( X>k \right)}$
      &
      $P\left( X<\mu-k \right)=\blanc{P\left( X>\mu+k \right)}$
      &
      $P\left( X<\mu \right)=P\left(X>\mu\right)=\blanc{0,5}$
      \\
      \begin{tikzpicture}[scale=1]
        \begin{axis}[
            no markers,
            domain=0:6,
            samples=100,
            ymin=0,
            axis lines*=left,
            every axis y label/.style={at=(current axis.above origin),anchor=south},
            every axis x label/.style={at=(current axis.right of origin),anchor=west},
            height=3.3cm,
            width=6cm,
            xtick=\empty,
            ytick=\empty,
            enlargelimits=false,
            clip=false,
            axis on top,
            grid = major,
            hide y axis
          ]

          \pgfmathsetmacro\valueA{gauss(1,1,1.7)}
          \pgfmathsetmacro\valueC{gauss(1,1,1)}


          \addplot [preaction={fill=yellow!25}, pattern=north west lines, domain=2:6] {gauss(x, 3, 1)} \closedcycle;
          \draw [very thick, red]  (axis cs:2,0) -- (axis cs:2,\valueA);
          \draw [very thick, red, dashed]  (axis cs:3,0) -- (axis cs:3,\valueC);
          \addplot [very thick,black] {gauss(x, 3, 1)};
          \node[below] at (axis cs:3, 0)  {$\mu$};
          \node[below] at (axis cs:2,0) {$k$};
          \draw[-latex] ($(axis cs:1, 0)+(axis cs:-1, .2)$) node[above]{$P\left(X<k\right)$} -- (axis cs:1.6, .07);
          \draw[-latex] ($(axis cs:4, 0)+(axis cs:1.2, .3)$) node[above]{$P\left(X>k\right)$} -- (axis cs:4, .07);
        \end{axis}
      \end{tikzpicture}
      &
      \hspace*{-1cm}\begin{tikzpicture}
        \begin{axis}[
            no markers,
            domain=0:6,
            samples=100,
            ymin=0,
            axis lines*=left,
            height=3.3cm,
            width=8cm,
            xtick=\empty,
            ytick=\empty,
            enlargelimits=false,
            clip=false,
            hide y axis
          ]

          \pgfmathsetmacro\valueA{gauss(1,1,1.7)}
          \pgfmathsetmacro\valueC{gauss(1,1,1)}


          \addplot [preaction={fill=yellow!25}, pattern=north west lines, domain=0:2] {gauss(x, 3, 1)} \closedcycle;
          \addplot [preaction={fill=yellow!25}, pattern=north east lines, domain=4:6] {gauss(x, 3, 1)} \closedcycle;
          \draw [very thick, red]  (axis cs:2,0) -- (axis cs:2,\valueA);
          \draw [very thick, red]  (axis cs:4,0) -- (axis cs:4,\valueA);
          \draw [very thick, red, dashed]  (axis cs:3,0) -- (axis cs:3,\valueC);
          \addplot [very thick,black] {gauss(x, 3, 1)};
          \node[below] at (axis cs:3, 0)  {$\mu$};
          \node[below] at (axis cs:2,0) {$\mu-k$};
          \node[below] at (axis cs:4,0) {$\mu+k$};
          \draw[-latex] (axis cs:.3, .3) node[above]{$P\left(X<\mu-k\right)$} -- (axis cs:1.6, .07);
          \draw[-latex] (axis cs:5.6, .3) node[above]{$P\left(X>\mu+k\right)$} -- (axis cs:4.4, .07);
          \draw [decorate,decoration={brace,amplitude=5pt,mirror,raise=5pt},yshift=-2ex] (axis cs: 2, 0) -- (axis cs: 3, 0) node[midway, below=2ex]{$k$};
          \draw [decorate,decoration={brace,amplitude=5pt,mirror,raise=5pt},yshift=-2ex] (axis cs: 3, 0) -- (axis cs: 4, 0) node[midway, below=2ex]{$k$};
        \end{axis}
      \end{tikzpicture}
      &
      \begin{tikzpicture}[scale=1]
        \begin{axis}[
            no markers,
            domain=0:6,
            samples=100,
            ymin=0,
            axis lines*=left,
            every axis y label/.style={at=(current axis.above origin),anchor=south},
            every axis x label/.style={at=(current axis.right of origin),anchor=west},
            height=3.3cm,
            width=6cm,
            xtick=\empty,
            ytick=\empty,
            enlargelimits=false,
            clip=false,
            axis on top,
            grid = major,
            hide y axis
          ]

          \pgfmathsetmacro\valueC{gauss(1,1,1)}


          \addplot [preaction={fill=yellow!25}, pattern=north west lines, domain=3:6] {gauss(x, 3, 1)} \closedcycle;
          \draw [very thick, red, dashed]  (axis cs:3,0) -- (axis cs:3,\valueC);
          \addplot [very thick,black] {gauss(x, 3, 1)};
          \node[below] at (axis cs:3, 0)  {$\mu$};
          \node[below] at (axis cs:2,0) {$k$};
          \draw[-latex] (axis cs:0.3, .3) node[above]{$P\left(X<\mu\right)$} -- (axis cs:1.6, .07);
          \draw[-latex] ($(axis cs:5, 0)+(axis cs:1.2, .3)$) node[above]{$P\left(X>\mu\right)$} -- (axis cs:4, .07);
        \end{axis}
      \end{tikzpicture}
      \\
    \end{tabularx}
  \end{center}
\end{propriete}

\begin{exercice}[D'après le sujet de bac Nouvelle Calédonie, 16 novembre 2016]
  Un producteur vend des yaourts chaque samedi sur un marché. On note $X$ la variable aléatoire, qui, à chaque semaine, associe le nombre de yaourts vendus au marché. On admet que $X$ suit la loi normale d'espérance $\mu=180$ et d'écart type $\sigma=30$.

  \begin{enumerate}
    \item  Calculer à l'aide de la calculatrice, la probabilité arrondie au millième que le nombre de yaourts vendus soit inférieur ou égal à 150.
  \end{enumerate}

  On donne la courbe de densité de la loi normale d'espérance $\mu=180$ et d'écart type $\sigma=30$.

  \begin{tikzpicture}
    \begin{axis}[
        no markers,
        domain=0:350,
        samples=100,
        ymin=0,
        axis lines=center,
        xlabel={x},
        ylabel={y},
        height=6cm,
        width=18cm,
        xtick={0, 50, ..., 350},
        ytick=\empty,
        enlargelimits=false,
        clip=false,
      ]

      \pgfmathsetmacro\valueA{gauss(135, 180, 30)}
      \pgfmathsetmacro\valueC{gauss(180, 180, 30)}


      \addplot [pattern=north west lines, domain=135:180] {gauss(x, 180, 30)} \closedcycle;
      \addplot [very thick,black] {gauss(x, 180, 30)};
      \draw (axis cs: 10, \valueC) node[anchor=north west]{$P\left(135\leq X\leq180\right)\approx0,433$};
    \end{axis}
  \end{tikzpicture}

  \begin{enumerate}
      \setcounter{enumi}{1}
    \item Sur ce graphique, on peut lire: $P(135 \leqslant X \leqslant 180) \approx 0,433$. Interpréter ce résultat
    \item En déduire $P(180 \leqslant X \leqslant 225)$ et $P(X \geqslant 225)$.
    \item  Ce samedi, le producteur n'a apporté que 225 yaourts au marché. Quelle est la
      probabilité qu'il ait besoin de compléter son stock ?
  \end{enumerate}

\end{exercice}

\section{Calculatrice}

TODO

\section{Intervalle de fluctuation}

TODO
\end{document}
