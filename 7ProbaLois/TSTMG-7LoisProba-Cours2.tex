%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Copyright 2017-2020 Louis Paternault --- http://ababsurdo.fr
%
% Publié sous licence Creative Commons Attribution-ShareAlike 4.0 International (CC BY-SA 4.0)
% http://creativecommons.org/licenses/by-sa/4.0/deed.fr
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% Pour compiler :
%$ lualatex $basename

\documentclass[12pt]{article}

\usepackage{1920-pablo}
\usepackage{1920-pablo-paternault}
\usepackage[a5paper, bottom=1.5cm, left=1cm, right=1cm]{geometry}

\usepackage{fancyhdr}
\pagestyle{fancy}
\fancyhead[L]{\textsc{Chapitre 7 --- Lois de probabilité}}
\fancyhead[R]{\textsc{Cours 2}}
\fancyfoot[C]{}

\setlength{\parindent}{0pt}

\usepackage{tabularx}
\usepackage{mdframed}

\usepackage{qrcode}

\renewcommand{\blanc}[1]{\textcolor{red}{\large #1}}

\pgfmathdeclarefunction{gauss}{3}{%
  \pgfmathparse{1/(#3*sqrt(2*pi))*exp(-((#1-#2)^2)/(2*#3^2))}%
}

\begin{document}

\setcounter{section}{1}

Si ce n'est pas encore fait, commencez par faire l'activité d'introduction du fichier \texttt{TSTMG-7LoisProba-Cours2-intro.pdf}.

\section{Loi Normale}

Une variable aléatoire de loi binomiale $n$ et $p$, lorsque $n$ est assez grand et que $n\times p$ est supérieur à 5, peut être modélisée par une \emph{loi normale} de même espérance et écart-type.

\begin{definition}[Rappels]
  Étant donné une variable aléatoire $X$ :
  \begin{itemize}
    \item son \blanc{espérance} est la moyenne des valeurs qu'elle peut prendre ;
    \item son \blanc{écart-type} mesure la dispersion des valeurs qu'elle peu prendre (plus il est petit, plus ses valeurs sont proches de son espérance ; plus il est grand, plus ses valeurs peuvent être éloignées de son espérance).
  \end{itemize}
\end{definition}

\begin{propriete}
  La courbe de densité d'une variable aléatoire $X$ suit une loi normale d'espérance $\mu$ (« mu ») et d'écart-type $\sigma$ (« sigma ») est la \blanc{courbe en cloche}.

  \begin{itemize}
    \item Elle est symétrique par rapport à la droite verticale d'équation $x=\mu$.
    \item Pour des réels $k$ et $l$, les probabilités $P\left( X<k \right)$, $P\left( X>k \right)$ et $P\left( k<X<l \right)$ peuvent se lire (en théorie) en mesurant l'aire sous la courbe.
  \end{itemize}
\end{propriete}

\begin{tabularx}{\linewidth}{ccc}
  \begin{tikzpicture}
    \begin{axis}[
        no markers,
        domain=0:6,
        samples=100,
        scale=.7,
        ymin=0,
        axis lines*=left,
        every axis y label/.style={at=(current axis.above origin),anchor=south},
        every axis x label/.style={at=(current axis.right of origin),anchor=west},
        height=3.3cm,
        width=6cm,
        xtick=\empty,
        ytick=\empty,
        enlargelimits=false,
        clip=false,
        axis on top,
        grid = major,
        hide y axis
      ]

      \pgfmathsetmacro\valueA{gauss(1,1,1.7)}
      \pgfmathsetmacro\valueC{gauss(1,1,1)}


      \addplot [preaction={fill=yellow!25}, pattern=north west lines, domain=0:2] {gauss(x, 3, 1)} \closedcycle;
      \draw [very thick, red]  (axis cs:2,0) -- (axis cs:2,\valueA);
      \draw [very thick, red, dashed]  (axis cs:3,0) -- (axis cs:3,\valueC);
      \addplot [very thick,black] {gauss(x, 3, 1)};
      \node[below] at (axis cs:3, 0)  {$\mu$};
      \node[below] at (axis cs:2,0) {$k$};
      \draw[-latex] ($(axis cs:1, 0)+(axis cs:-.5, .4)$) node[above]{$P\left(X<k\right)$} -- (axis cs:1.6, .07);
    \end{axis}
  \end{tikzpicture}
  &
  \begin{tikzpicture}
    \begin{axis}[
        no markers,
        scale=0.7,
        domain=0:6,
        samples=100,
        ymin=0,
        axis lines*=left,
        every axis y label/.style={at=(current axis.above origin),anchor=south},
        every axis x label/.style={at=(current axis.right of origin),anchor=west},
        height=3.3cm,
        width=6cm,
        xtick=\empty,
        ytick=\empty,
        enlargelimits=false,
        clip=false,
        axis on top,
        grid = major,
        hide y axis
      ]

      \pgfmathsetmacro\valueA{gauss(1,1,1.7)}
      \pgfmathsetmacro\valueC{gauss(1,1,1)}


      \addplot [preaction={fill=yellow!25}, pattern=north west lines, domain=2:6] {gauss(x, 3, 1)} \closedcycle;
      \draw [very thick, red]  (axis cs:2,0) -- (axis cs:2,\valueA);
      \draw [very thick, red, dashed]  (axis cs:3,0) -- (axis cs:3,\valueC);
      \addplot [very thick,black] {gauss(x, 3, 1)};
      \node[below] at (axis cs:3, 0)  {$\mu$};
      \node[below] at (axis cs:2,0) {$k$};
      \draw[-latex] ($(axis cs:4, 0)+(axis cs:1, .4)$) node[above]{$P\left(k<X\right)$} -- (axis cs:4, .07);
    \end{axis}
  \end{tikzpicture}
  &
  \begin{tikzpicture}
    \begin{axis}[
        scale=.7,
        no markers,
        domain=0:6,
        samples=100,
        ymin=0,
        axis lines*=left,
        every axis y label/.style={at=(current axis.above origin),anchor=south},
        every axis x label/.style={at=(current axis.right of origin),anchor=west},
        height=3.3cm,
        width=6cm,
        xtick=\empty,
        ytick=\empty,
        enlargelimits=false,
        clip=false,
        axis on top,
        grid = major,
        hide y axis
      ]

      \pgfmathsetmacro\valueA{gauss(2,3,1)}
      \pgfmathsetmacro\valueB{gauss(4.5,3,1)}
      \pgfmathsetmacro\valueC{gauss(3,3,1)}


      \addplot [preaction={fill=yellow!25}, pattern=north west lines, domain=2:4.5] {gauss(x, 3, 1)} \closedcycle;
      \draw [very thick, red]  (axis cs:2,0) -- (axis cs:2,\valueA);
      \draw [very thick, red]  (axis cs:4.5,0) -- (axis cs:4.5,\valueB);
      \draw [very thick, red, dashed]  (axis cs:3,0) -- (axis cs:3,\valueC);
      \addplot [very thick,black] {gauss(x, 3, 1)};
      \node[below] at (axis cs:3, 0)  {$\mu$};
      \node[below] at (axis cs:2,0) {$k$};
      \node[below] at (axis cs:4.5,0) {$l$};
      \draw[-latex] ($(axis cs:4, 0)+(axis cs:1, .4)$) node[above]{$P\left(k<X<l\right)$} -- (axis cs:4, .07);
    \end{axis}
  \end{tikzpicture}
  \\
\end{tabularx}

\begin{propriete}
  Pour tout réel $k$, on a :

  $P\left( X=k \right)=0$
  \hspace{\stretch{1}}
  $P\left( X<k \right)=P\left( X\leq k \right)$
  \hspace{\stretch{1}}
  $P\left( X>k \right)=P\left( X\geq k \right)$

  \emph{En d'autres termes : la probablitié d'obtenir \emph{exactement} une certaine valeur est nulle.}
\end{propriete}

\begin{propriete}~

  \begin{center}
    \begin{tabularx}{\textwidth}{*{2}{|>{\centering \arraybackslash}X}|}
      \hline
      \begin{tikzpicture}
        \begin{axis}[
            no markers,
            domain=0:6,
            samples=100,
            ymin=0,
            axis lines*=left,
            every axis y label/.style={at=(current axis.above origin),anchor=south},
            every axis x label/.style={at=(current axis.right of origin),anchor=west},
            height=3.3cm,
            width=6cm,
            xtick=\empty,
            ytick=\empty,
            enlargelimits=false,
            clip=false,
            axis on top,
            grid = major,
            hide y axis
          ]

          \pgfmathsetmacro\valueC{gauss(1,1,1)}


          \addplot [preaction={fill=yellow!25}, pattern=north west lines, domain=3:6] {gauss(x, 3, 1)} \closedcycle;
          \draw [very thick, red, dashed]  (axis cs:3,0) -- (axis cs:3,\valueC);
          \addplot [very thick,black] {gauss(x, 3, 1)};
          \node[below] at (axis cs:3, 0)  {$\mu$};
          \node[below] at (axis cs:2,0) {$k$};
          \draw[-latex] (axis cs:0.3, .3) node[above]{$P\left(X<\mu\right)$} -- (axis cs:1.6, .07);
          \draw[-latex] ($(axis cs:5, 0)+(axis cs:0.8, .3)$) node[above]{$P\left(X>\mu\right)$} -- (axis cs:4, .07);
        \end{axis}
      \end{tikzpicture}
      &
      \begin{tikzpicture}
        \begin{axis}[
            no markers,
            domain=0:6,
            samples=100,
            ymin=0,
            axis lines*=left,
            every axis y label/.style={at=(current axis.above origin),anchor=south},
            every axis x label/.style={at=(current axis.right of origin),anchor=west},
            height=3.3cm,
            width=6cm,
            xtick=\empty,
            ytick=\empty,
            enlargelimits=false,
            clip=false,
            axis on top,
            grid = major,
            hide y axis
          ]

          \pgfmathsetmacro\valueA{gauss(1,1,1.7)}
          \pgfmathsetmacro\valueC{gauss(1,1,1)}


          \addplot [preaction={fill=yellow!25}, pattern=north west lines, domain=2:6] {gauss(x, 3, 1)} \closedcycle;
          \draw [very thick, red]  (axis cs:2,0) -- (axis cs:2,\valueA);
          \draw [very thick, red, dashed]  (axis cs:3,0) -- (axis cs:3,\valueC);
          \addplot [very thick,black] {gauss(x, 3, 1)};
          \node[below] at (axis cs:3, 0)  {$\mu$};
          \node[below] at (axis cs:2,0) {$k$};
          \draw[-latex] ($(axis cs:1, 0)+(axis cs:-1, .2)$) node[above]{$P\left(X<k\right)$} -- (axis cs:1.6, .07);
          \draw[-latex] ($(axis cs:4, 0)+(axis cs:1.2, .3)$) node[above]{$P\left(X>k\right)$} -- (axis cs:4, .07);
        \end{axis}
      \end{tikzpicture}
      \\
      $P\left( X<\mu \right)=P\left(X>\mu\right)=0,5$
      &
      $P\left( X<k \right)=1-P\left( X>k \right)$
      \\
      \hline
      \multicolumn{2}{|c|}{
        \begin{tikzpicture}
          \begin{axis}[
              no markers,
              domain=0:6,
              samples=100,
              ymin=0,
              axis lines*=left,
              height=3.3cm,
              width=8cm,
              xtick=\empty,
              ytick=\empty,
              enlargelimits=false,
              clip=false,
              hide y axis
            ]

            \pgfmathsetmacro\valueA{gauss(1,1,1.7)}
            \pgfmathsetmacro\valueC{gauss(1,1,1)}


            \addplot [preaction={fill=yellow!25}, pattern=north west lines, domain=0:2] {gauss(x, 3, 1)} \closedcycle;
            \addplot [preaction={fill=yellow!25}, pattern=north east lines, domain=4:6] {gauss(x, 3, 1)} \closedcycle;
            \draw [very thick, red]  (axis cs:2,0) -- (axis cs:2,\valueA);
            \draw [very thick, red]  (axis cs:4,0) -- (axis cs:4,\valueA);
            \draw [very thick, red, dashed]  (axis cs:3,0) -- (axis cs:3,\valueC);
            \addplot [very thick,black] {gauss(x, 3, 1)};
            \node[below] at (axis cs:3, 0)  {$\mu$};
            \node[below] at (axis cs:2,0) {$\mu-k$};
            \node[below] at (axis cs:4,0) {$\mu+k$};
            \draw[-latex] (axis cs:.3, .3) node[above]{$P\left(X<\mu-k\right)$} -- (axis cs:1.6, .07);
            \draw[-latex] (axis cs:5.6, .3) node[above]{$P\left(X>\mu+k\right)$} -- (axis cs:4.4, .07);
            \draw [decorate,decoration={brace,amplitude=5pt,mirror,raise=5pt},yshift=-2ex] (axis cs: 2, 0) -- (axis cs: 3, 0) node[midway, below=2ex]{$k$};
            \draw [decorate,decoration={brace,amplitude=5pt,mirror,raise=5pt},yshift=-2ex] (axis cs: 3, 0) -- (axis cs: 4, 0) node[midway, below=2ex]{$k$};
          \end{axis}
        \end{tikzpicture}
      }\\
      \multicolumn{2}{|c|}{$P\left( X<\mu-k \right)=P\left( X>\mu+k \right)$}
      \\
      \hline
    \end{tabularx}
  \end{center}
\end{propriete}

\pagebreak

\begin{exercice*}[D'après le sujet de bac Nouvelle Calédonie, 16 novembre 2016]
  Un producteur vend des yaourts chaque samedi sur un marché. On note $X$ la variable aléatoire, qui, à chaque semaine, associe le nombre de yaourts vendus au marché. On admet que $X$ suit la loi normale d'espérance $\mu=180$ et d'écart type $\sigma=30$.

  \begin{enumerate}
    \item \emph{Calculer sans l'aide de la calculatrice, la probabilité arrondie au millième que le nombre de yaourts vendus soit inférieur ou égal à 180.}

      Puisque 180 est l'espérance de la loi normble, on a $P(X<180)=0,5$ : la probabilité que le nombre de yaourts vendus soit inférieur ou égal à 180 est 0,5 (soit une chance sur deux).
  \end{enumerate}

  On donne la courbe de densité de la loi normale d'espérance $\mu=180$ et d'écart type $\sigma=30$.

  \begin{tikzpicture}
    \begin{axis}[
        no markers,
        domain=0:350,
        samples=100,
        ymin=0,
        xscale=.8,
        axis lines=center,
        xlabel={x},
        ylabel={y},
        height=6cm,
        width=18cm,
        xtick={0, 50, ..., 350},
        ytick=\empty,
        enlargelimits=false,
        clip=false,
      ]

      \pgfmathsetmacro\valueA{gauss(135, 180, 30)}
      \pgfmathsetmacro\valueC{gauss(180, 180, 30)}


      \addplot [pattern=north west lines, domain=135:180] {gauss(x, 180, 30)} \closedcycle;
      \addplot [very thick,black] {gauss(x, 180, 30)};
      \draw (axis cs: 10, {.7*\valueC}) node[anchor=north west]{$P\left(135\leq X\leq180\right)\approx0,433$};
    \end{axis}
  \end{tikzpicture}

  \begin{enumerate}
      \setcounter{enumi}{1}
    \item \emph{Sur ce graphique, on peut lire: $P(135 \leqslant X \leqslant 180) \approx 0,433$. Interpréter ce résultat.}

      La probabilité que le nombre de yaourts vendus soit comprise entre 135 et 180 est égale à 0,433 (soit environ 43,3\%).
    \item \emph{En déduire $P(180 \leqslant X \leqslant 225)$ et $P(X \geqslant 225)$.}

      On remarque que $180-135=45$, et $225-180=45$. Donc les intervalles $\intervallef{135;180}$ et $\intervallef{180;225}$ sont symétriques par rapport à 180. Puisque l'espérance de la loi normale est 180, alors la courbe est elle aussi symétrique par rapport à 180, et les probabilités correspondantes sont égales :
      \[
        P(180\leq X\leq225)=P(135\leq X\leq180)=0,433
      \]

      De plus, on remarque que $P(X\geq225)=P(X\leq135)$ (pour les mêmes raisons de symétrie). Donc :
      \begin{align*}
        P(X\leq 135)+P(135\leq X\leq180)+\phantom{XXX}&\\
        P(180\leq X\leq225)+P(X\geq225)&=1\\
        P(X\geq 225)+0,433+0,433+P(X\geq225)&=1\\
        2P(X\geq 225)&=1-0,433-0,433\\
        2P(X\geq 225)&=0,134\\
        P(X\geq 225)&=\frac{0,134}{2}\\
        P(X\geq 225)&=0,067
      \end{align*}
    \item \emph{ Ce samedi, le producteur n'a apporté que 225 yaourts au marché. Quelle est la probabilité qu'il ait besoin de compléter son stock ?.}

      On a montré que $P(X\geq225)=0,067$ : la probabilité que le nombre de yaourts vendus soit supérieur à 225 (et donc qu'il ait besoin de compléter son stock) est égale à 0,067 (soit 6,7\% environ).
  \end{enumerate}

\end{exercice*}

\end{document}
