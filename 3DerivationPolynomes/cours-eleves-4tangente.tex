%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Copyright 2017-2019 Louis Paternault --- http://ababsurdo.fr
%
% Publié sous licence Creative Commons Attribution-ShareAlike 4.0 International (CC BY-SA 4.0)
% http://creativecommons.org/licenses/by-sa/4.0/deed.fr
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% Pour compiler :
%$ lualatex $basename

\documentclass[11pt]{article}

\usepackage{1920-pablo}
\usepackage{1920-pablo-paternault}
\usepackage[a5paper, margin=.5cm]{geometry}

\setlength{\parindent}{0cm}
\usepackage{multicol}

\begin{document}
\setcounter{section}{3}
\section{Tangente}

\subsection{Définition et Équation}

\begin{multicols}{2}
\begin{definition}
Soit $f$ une fonction dérivable, et $A$ un point de la courbe d'abscisse $a$. La \emph{tangente} à la courbe de $f$ au point d'abscisse $a$ est la droite passant par \blanc{le point $A$} et de coefficient directeur \blanc{$f'(a)$}.
\end{definition}

\begin{propriete}[Équation de la tangente]
Soit $f$ une fonction dérivable en un point $A$ d'abscisse $a$. L'équation réduite de la tangente à la courbe de $f$ au point d'abscisse $a$ est :
\[\blanc{y=f'(a)\left(x-a\right)+f(a)}\]
\end{propriete}

\columnbreak

\begin{center}
\begin{tikzpicture}[ultra thick, scale=1.0]
    \draw[dotted, gray] (-2, -2) grid (4, 3);
    \draw[-latex] (-2,0) -- (4,0);
    \draw[-latex] (0,-2) -- (0,3);
    \draw (0, 0) node[below left]{$\mathcal{O}$};
    \draw (0, 1) node[left]{1};
    \draw (1, 0) node[below]{1};
    \draw[color=blue,domain=-2:4] plot (\x, {.5*\x*\x-\x-1});
    \draw[domain=1:4] plot (\x, {\x-3});
    \draw (3, 0.5) node[above left]{$\mathcal{G}_f$};
    \draw (2, -1) node{\bullet} node[below right]{$A$};
  \end{tikzpicture}
\end{center}

\end{multicols}


\begin{nexemple}
  On considère la fonction $f$ définie sur $\mathbb{R}$ par : $f(x)=\frac{x^3}{3}-x^2+2x-3$.
\begin{enumerate}
\item Calculer $f'(x)$.
\item Déterminer l'équation de la tangente à la courbe de $f$ au point d'abscisse $2$.
\end{enumerate}
\end{nexemple}

\subsection{Tracé et Lecture graphique}

\begin{methode}[Tracé d'une tangente]
On cherche à tracer la tangente à la courbe $f$ au point d'abscisse $a$.

\begin{enumerate}
\item Repérer le point $A$ de la courbe d'abscisse $a$.
\item Calculer l'expression de $f'(x)$, puis calculer le nombre $f'(a)$.
\item À partir du point $A$, se déplacer d'une unité vers la droite, et de $f'(a)$ unités vers le haut (ou vers le bas si $f'(a)$ est négatif). Tracer la droite passant par $A$ et ce nouveau point.
\end{enumerate}
\end{methode}

\begin{nexemple}
Voici la courbe de la fonction $f:x\mapsto \frac{x^3}{4}-\frac{3}{4}x^2-x+1$.

\begin{multicols}{2}
\begin{tikzpicture}[ultra thick, xscale=1.0, yscale=.6]
    \draw[dotted, gray] (-2, -3) grid (4, 3);
    \draw[-latex] (-2,0) -- (4,0);
    \draw[-latex] (0,-3) -- (0,3);
    \draw (0, 0) node[below left]{$\mathcal{O}$};
    \draw (0, 1) node[left]{1};
    \draw (1, 0) node[below]{1};
    \clip (-3, -3) rectangle (4, 3);
    \draw[color=blue,samples=50,domain=-2:4] plot (\x, {\x*\x*\x/4-3/4*\x*\x-\x+1});
  \end{tikzpicture}

\begin{enumerate}
\item Tracer la tangente à la courbe au point d'abscisse $-1$.
\item Tracer la tangente à la courbe au point d'abscisse $3$.
\end{enumerate}
\end{multicols}
\end{nexemple}


\begin{methode}[Lecture graphique d'un nombre dérivé]
  Soit une fonction $f$, définie et dérivable sur son ensemble de définition. On a représenté la courbe de $f$, et la tangente à la courbe au point d'abscisse $a$.

  Le nombre $f'(a)$ est \blanc{le coefficient directeur de la tangente à la courbe de $f$ au point d'abscisse $a$}. Pour lire graphiquement $f'(a)$, il faut donc calculer le coefficient directeur de cette tangente.
  \begin{enumerate}
    \item On repère cette tangente.
    \item On choisit deux points distincts sur cette tangente.
    \item On lit graphiquement $\Delta{}x$ (différence des abscisses des deux points) et $\Delta{}y$ (différence entre les abscisses et les ordonnées des deux points).
    \item On calcule $f'(a)=\frac{\Delta{}y}{\Delta{}x}$.
  \end{enumerate}

  Cas particulier : Si la tangente est parallèle à l'axe des abscisses, $f'(a)=0$.
\end{methode}

\begin{nexemple}~
\begin{center}
\begin{tikzpicture}[ultra thick, scale=1.2]
    \draw[dotted, gray] (-1, -3) grid (7, 5);
    \draw[-latex] (-1,0) -- (7,0);
    \draw[-latex] (0,-3) -- (0,5);
    \draw (0, 0) node[below left]{$\mathcal{O}$};
    \draw (0, 1) node[left]{1};
    \draw (1, 0) node[below]{1};
    %\clip (-3, -3) rectangle (4, 3);
    \draw[color=blue,samples=50,domain=-1:7] plot (\x, {\x*\x/4-\x-1});
    \draw[gray, latex-latex, domain=-.7:1.6] plot (\x, {-\x-1});
    \draw[gray, latex-latex, domain=0.5:3.5] plot (\x, {-2});
    \draw[gray, latex-latex, domain=3.6:7] plot (\x, {2*\x-10});
    \draw[gray] (0, -1) node{\Large $\times$};
    \draw[gray] (2, -2) node{\Large $\times$};
    \draw[gray] (6,  2) node{\Large $\times$};
  \end{tikzpicture}
\end{center}

  \begin{enumerate}
    \item Lire graphiquement la valeur de $f'(6)$ et $f'(2)$.
    \item Déterminer l'équation réduite de la tangente à la courbe de $f$ au point d'abscisse 6.
  \end{enumerate}
\end{nexemple}

\end{document}
