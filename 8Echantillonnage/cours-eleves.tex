%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Copyright 2017 Louis Paternault --- http://ababsurdo.fr
%
% Publié sous licence Creative Commons Attribution-ShareAlike 4.0 International (CC BY-SA 4.0)
% http://creativecommons.org/licenses/by-sa/4.0/deed.fr
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% Pour compiler :
%$ lualatex $basename

\documentclass{article}

\usepackage{1920-pablo}
\usepackage{1920-pablo-paternault}

\usepackage{tabularx}
\usepackage{numprint}

\usepackage[a5paper, margin=0.6cm]{geometry}

\pgfmathdeclarefunction{gauss}{3}{%
  \pgfmathparse{1/(#3*sqrt(2*pi))*exp(-((#1-#2)^2)/(2*#3^2))}%
}
\usetikzlibrary{patterns}

\setlength\parindent{0mm}

\begin{document}

\begin{center}
\textsc{\Large Ch. 8 --- Échantillonnage}
\hrule
\end{center}

\section{Introduction}

\subsection{Intervalle de confiance}

Un \emph{intervalle de confiance} permet de répondre au genre de questions suivantes.

\begin{question*}
Pour connaître les intentions de vote des français, on a interrogé un échantillon représentatif de 1000 personnes annonçant aller voter, et 59\,\% d'entre elles ont annoncé voter pour Emmanuel Macron.

Peut-on affirmer qu'Emmanuel Macron va être élu ?
\end{question*}
\begin{reponse*}
L'intervalle de confiance à 95\,\% de la proportion de votants pour Emmanuel Macron est $\left[0,59-\frac{1}{\sqrt{1000}} ; 0,59+\frac{1}{\sqrt{1000}} \right]$, soit $\left[0,55; 0,63\right]$. On peut donc affirmer, avec un risque d'erreur inférieur à 5\,\%, qu'Emmanuel Macron va être élu.
\end{reponse*}

\subsection{Intervalle de fluctuation}

Un \emph{intervalle de fluctuation} permet de répondre au genre de questions suivantes.
\begin{question*}
Un joueur malchanceux se demande si la pièce avec laquelle il joue est faussée ou non. Pour vérifier, il la lance 500 fois, et a obtenu 287 fois « Pile ».

Peut-il affirmer que le dé est faussé ?
\end{question*}
\begin{reponse*}
L'intervalle de fluctuation à 95\,\% de la proportion de « Pile » sur 500 lancers  d'une pièce équilibrée est $\left[0,50-\frac{1}{\sqrt{500}} ; 0,50+\frac{1}{\sqrt{500}} \right]$, soit $\left[0,45 ; 0,55\right]$. Cela signifie qu'en lancant une pièce équilibrée 500 fois, il y a 95\,\% de chances d'obtenir une proportion de « Pile » comprise entre 0,45 et 0,55.

La proportion de « Pile » obtenue par le joueur est $287/500$ soit environ $0,57$, ce qui est à l'extérieur de l'intervalle de fluctuation. On peut, avec une probabilité d'erreur inférieure à 5\,\%, rejeter l'hypothèse que la pièce est équilibrée (en d'autres termes, il est probable qu'elle soit faussée).
\end{reponse*}

\section{Cours}

\subsection{Loi normale}

On considère une variable aléatoire $X$ suivant une loi normale d'espérance $\mu$ et d'écart-type $\sigma$.

\begin{propriete}
La probabilité que $X$ soit comprise entre $\mu-2\sigma$ et $\mu+2\sigma$ est supérieure à 95\,\%. En d'autres termes :
\[P\left(\mu-2\sigma\leq X\leq\mu+2\sigma\right)>0,95\]
\end{propriete}

\begin{definition}
L'intervalle $\left[\mu-2\sigma;\mu+2\sigma\right]$ est appelé \emph{intervalle de fluctuation} de la variable $X$ à 95\,\%.
\end{definition}

\begin{center}
  \begin{tikzpicture}[scale=1]
    \begin{axis}[
        no markers,
        domain=0:6,
        samples=100,
        ymin=0,
        axis lines*=left,
        every axis y label/.style={at=(current axis.above origin),anchor=south},
        every axis x label/.style={at=(current axis.right of origin),anchor=west},
        height=3.3cm,
        width=6cm,
        xtick=\empty,
        ytick=\empty,
        enlargelimits=false,
        clip=false,
        axis on top,
        grid = major,
        hide y axis
      ]

      \pgfmathsetmacro\valueA{gauss(1,3,1)}
      \pgfmathsetmacro\valueB{gauss(5,3,1)}
      \pgfmathsetmacro\valueC{gauss(3,3,1)}


      \addplot [preaction={fill=yellow!25}, pattern=north west lines, domain=1:5] {gauss(x, 3, 1)} \closedcycle;
      \draw [very thick, red]  (axis cs:1,0) -- (axis cs:1,\valueA);
      \draw [very thick, red]  (axis cs:5,0) -- (axis cs:5,\valueB);
      \draw [very thick, red, dashed]  (axis cs:3,0) -- (axis cs:3,\valueC);
      \addplot [very thick,black] {gauss(x, 3, 1)};
      \node[below] at (axis cs:3, 0)  {$\mu$};
      \node[below] at (axis cs:1,0) {$\mu-2\sigma$};
      \node[below] at (axis cs:5,0) {$\mu+2\sigma$};
    \end{axis}
  \end{tikzpicture}
\end{center}

\begin{remarque}
La notion d'\emph{intervalle de confiance} n'est pas utilisée avec la loi normale en terminale \textsc{stmg}.
\end{remarque}

\subsection{Intervalle de confiance}

On cherche à estimer la proportion $p$, inconnue, d'un caractère d'une population (la probabilité d'obtenir Pile pour une pièce truquée ; la proportion de votants pour un candidat à une élection ; la proportion de pièces défectueuses produites par une machine ; etc.).

\begin{defprop}
On considère un échantillon de taille $n$ dans une population, et on calcule la fréquence observée $f$ du caractère dans cet échantillon.

Alors on appelle \emph{intervalle de confiance} de la proportion $p$ au niveau de 95\,\% l'intervalle :

\[\left[f-\frac{1}{\sqrt{n}};f+\frac{1}{\sqrt{n}}\right]\]

Dans au moins 95\,\% des cas, la proportion $p$ se trouve dans cet intervalle.
\end{defprop}

\subsection{Intervalle de fluctuation}

On connait la probabilité d'apparition d'un caractère $p$ dans une population (la probabilité d'obtenir Pile pour une pièce équilibrée ; la proportion de pièces défectueuses produites par une machine ; la proportion d'électeurs ayant voté pour un candidat à une élection ; etc.), et on souhaite estimer, pour un échantillon de taille $n$, quelle va être la fréquence observée.

\begin{defprop}
On connait la proportion $p$ d'apparition d'un caractère dans une population, et on considère un échantillon de taille $n$ de cette population.

Alors on appelle \emph{intervalle de fluctuation} à 95\,\% de la fréquence $f$ l'intervalle :

\[\left[p-\frac{1}{\sqrt{n}};p+\frac{1}{\sqrt{n}}\right]\]

La probabilité que la fréquence observée $f$ soit dans cet intervalle est supérieure à 95\,\%.
\end{defprop}

\begin{propriete}[Prise de décision]
On suppose que la proportion d'un caractère dans une population est $p$.
\begin{itemize}
\item Si la fréquence observée $f$ n'est pas dans l'intervalle de fluctuation, alors l'hypothèse que la proportion est $p$ dans l'ensemble de la population est rejetée, avec un risque d'erreur de 5\,\%.
\item Si la fréquence observée $f$ est dans l'intervalle de fluctuation, alors l'hypothèse n'est pas rejetée.
\end{itemize}
\end{propriete}

\section{Exercices}

\subsection{Loi normale}

\begin{exercice}[Antilles-Guyane, 15 juin 2016]
On s'intéresse dans cette partie à la masse de pots de confiture.

On admet que la masse $M$ (en gramme) d'un pot de confiture prélevé au hasard dans le
stock est modélisée par une variable aléatoire suivant la loi normale de moyenne $250$ et
d'écart type $2,5$.

\begin{enumerate}
\item Donner la valeur de $p(245 \leqslant M \leqslant 255)$.
\item En déduire la probabilité qu'un pot de confiture ait une masse comprise entre $250$~g
et $255$~g.
\end{enumerate}
\end{exercice}

\begin{exercice}[Centres étrangers, 11 juin 2015]
Un laboratoire pharmaceutique fabrique des gélules contenant une substance S. La masse
de substance S, exprimée en milligrammes (mg), contenue dans une gélule est modélisée
par une variable aléatoire $X$ suivant la loi normale d'espérance $8,2$ et d'écart type $0,05$. 

La norme de fabrication impose que la masse de substance S dans une gélule soit comprise
entre 8,1~mg et 8,3~mg. La probabilité qu'une gélule soit hors norme après la fabrication
est :

\begin{tabularx}{\linewidth}{*{4}{X}}
\textbf{a.~~} 0,2&\textbf{b.~~} 0,05 &\textbf{c.~~}0,8 &\textbf{d.~~} 0,95
\end{tabularx}
\end{exercice}

\begin{exercice}[Métropole La Réunion, 16 juin 2016]
Un test d'aptitude est évalué sur $100$ points. Il faut obtenir au moins $60$~points pour le réussir.

Le score d'un candidat est modélisé par une variable aléatoire $X$ suivant une loi normale
d'espérance $\mu = 66$ et d'écart type $\sigma$ inconnu.

La probabilité, pour un candidat pris au hasard, d'obtenir un score compris entre $60$ et $72$
points est égale à $0,95$.

Parmi les valeurs ci-dessous, la plus proche de $\sigma$ est :

\begin{tabularx}{\linewidth}{*{4}{X}}
\textbf{a.~~} 3 &\textbf{b.~~} 6 &\textbf{c.~~} 5 &\textbf{d.~~} 9
\end{tabularx}
\end{exercice}

\subsection{Fluctuation d'échantillonnage}

\begin{exercice}[Polynésie, 7 juin 2016]
Dans une population, on estime qu’il naît 51\,\% de garçons et 49\,\% de filles.

L’intervalle de fluctuation à au moins 95\,\% de la fréquence des filles dans un échantillon de
100 naissances choisies au hasard sera :

\begin{tabularx}{\linewidth}{*{4}{X}} 
\textbf{a.~~} [0,48~;~0,50]&\textbf{b.~~}[0,39~;~0,59] &\textbf{c.~~}[0,41~;~0,61] &\textbf{d.~~}  [0,47~;~0,51]
\end{tabularx}
\end{exercice}

\subsection{Intervalle de confiance}

\begin{exercice}[Métropole La Réunion, 18 juin 2015]
Pour le repas du midi, les visiteurs restant toute la journée dans un parc peuvent :

\begin{itemize}
\item soit déjeuner dans l'un des restaurants du parc;
\item soit consommer, sur une aire de pique-nique, un repas qu'ils ont apporté.
\end{itemize}

La direction souhaite estimer la proportion $p$ de visiteurs déjeunant dans l'un des restaurants
du parc.

Un sondage est effectué à la sortie du parc : $247$ visiteurs parmi $625$ ont déjeuné dans l'un
des restaurants du parc.

Déterminer un intervalle de confiance au niveau de confiance de 95\,\% de la proportion $p$ de
visiteurs déjeunant dans l'un des restaurants du parc.
\end{exercice}

\begin{exercice}[Pondichéry, 26 avril 2017]
L’EFS affirme que dans une région donnée : \og 23\,\% de la population donne son sang au moins
une fois par an\fg.

On interroge au hasard un échantillon de \numprint{1 000} personnes habitant cette région. Parmi elles, $254$ ont donné au moins une fois leur sang au cours de la dernière année.

Peut-on mettre en doute l’affirmation de l’EFS ? Justifier la réponse à l’aide d’un intervalle de
fluctuation.
\end{exercice}

\begin{exercice}[Pondichéry, 22 avril 2016]
On considère un échantillon de \numprint{1500} personnes d'une ville, représentatif du comportement face au tri des déchets des habitants de cette ville.

Sachant que la probabilité qu'une personne prise au hasard dans cet échantillon trie le papier est $0,69$, estimer à l'aide d'un intervalle de confiance, au niveau de confiance de 95\,\%, la proportion des habitants de cette ville qui trient le papier.
\end{exercice}

\begin{exercice}[Métropole La Réunion, 16 juin 2016]
On formule l'hypothèse qu'après 3 semaines de campagne publicitaire, 11,2~\% des habitants d'une ville sont prêts à acheter la nouvelle marque de boisson d'une entreprise. L'agence de publicité décide de tester la validité de cette hypothèse.

Elle interroge un échantillon de 500 habitants de la ville pris au hasard. Parmi eux, 44 se disent effectivement prêts à acheter cette nouvelle boisson.

Au regard de ce sondage, peut-on rejeter, au risque de 5~\%, l'hypothèse formulée ci-dessus ? Justifier la réponse.
\end{exercice}

\begin{exercice}[Centres étrangers, 11 juin 2015]
Un maire souhaite estimer la proportion d'habitants de sa commune satisfaits des décisions
qu'il a prises depuis son élection. Un récent sondage effectué sur 800~habitants montre que
560~personnes sont satisfaites. 

Un intervalle de confiance au niveau de confiance 95\,\% pour la proportion d'opinions favorables est :

\begin{tabularx}{\linewidth}{*{4}{X}}
\textbf{a.~~} [0,66~;~0,74]&\textbf{b.~~} [0,69~;~0,71]&\textbf{c.~~} [0,60~;~0,80]&\textbf{d.~~} [0,71~;~0,79]
\end{tabularx}
\end{exercice}

\end{document}
