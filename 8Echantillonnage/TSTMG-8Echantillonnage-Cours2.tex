%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Copyright 2017-2020 Louis Paternault --- http://ababsurdo.fr
%
% Publié sous licence Creative Commons Attribution-ShareAlike 4.0 International (CC BY-SA 4.0)
% http://creativecommons.org/licenses/by-sa/4.0/deed.fr
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% Pour compiler :
%$ lualatex $basename

\documentclass[12pt]{article}

\usepackage{1920-pablo}
\usepackage{1920-pablo-paternault}
\usepackage[a5paper, bottom=1.0cm, left=.8cm, right=.8cm]{geometry}

\usepackage{fancyhdr}
\pagestyle{fancy}
\fancyhead[L]{\textsc{Chapitre 8 --- Échantillonnage}}
\fancyhead[R]{\textsc{Cours 2}}
\fancyfoot[C]{}

\setlength{\parindent}{0pt}

\usepackage{graph35}

\usepackage{tabularx}
\usepackage{mdframed}

\usepackage{qrcode}

\renewcommand{\blanc}[1]{\textcolor{red}{\large #1}}

\usepackage{environ}
\NewEnviron{solution}{%
  \begin{mdframed}
    \noindent
    \rotatebox[origin=c]{180}{%
      \noindent
      \begin{minipage}[t]{\linewidth}
        \BODY
      \end{minipage}%
    }%
  \end{mdframed}%
}%

\pgfmathdeclarefunction{gauss}{3}{%
  \pgfmathparse{1/(#3*sqrt(2*pi))*exp(-((#1-#2)^2)/(2*#3^2))}%
}
\usetikzlibrary{patterns}

\begin{document}

\setcounter{section}{2}
\section{Loi normale}
\begin{em}Dans votre cours, recopiez le titre de cette partie (\enquote{3 --- Loi normale}) ainsi que le contenu : la propriété, la définition, et l'exemple qui suivent.
\end{em}

On considère une variable aléatoire $X$ suivant une loi normale d'espérance $\mu$ et d'écart-type $\sigma$.

\begin{propriete}
La probabilité que $X$ soit comprise entre $\mu-2\sigma$ et $\mu+2\sigma$ est supérieure à 95\,\%. En d'autres termes :
\[P\left(\mu-2\sigma\leq X\leq\mu+2\sigma\right)>0,95\]
\end{propriete}

\begin{definition}
L'intervalle $\left[\mu-2\sigma;\mu+2\sigma\right]$ est appelé \emph{intervalle de fluctuation} de la variable $X$ à 95\,\%.
\end{definition}

\begin{center}
  \begin{tikzpicture}[scale=1]
    \begin{axis}[
        no markers,
        domain=0:6,
        samples=100,
        ymin=0,
        axis lines*=left,
        every axis y label/.style={at=(current axis.above origin),anchor=south},
        every axis x label/.style={at=(current axis.right of origin),anchor=west},
        height=3.3cm,
        width=6cm,
        xtick=\empty,
        ytick=\empty,
        enlargelimits=false,
        clip=false,
        axis on top,
        grid = major,
        hide y axis
      ]

      \pgfmathsetmacro\valueA{gauss(1,3,1)}
      \pgfmathsetmacro\valueB{gauss(5,3,1)}
      \pgfmathsetmacro\valueC{gauss(3,3,1)}


      \addplot [preaction={fill=yellow!25}, pattern=north west lines, domain=1:5] {gauss(x, 3, 1)} \closedcycle;
      \draw [very thick, red]  (axis cs:1,0) -- (axis cs:1,\valueA);
      \draw [very thick, red]  (axis cs:5,0) -- (axis cs:5,\valueB);
      \draw [very thick, red, dashed]  (axis cs:3,0) -- (axis cs:3,\valueC);
      \addplot [very thick,black] {gauss(x, 3, 1)};
      \node[below] at (axis cs:3, 0)  {$\mu$};
      \node[below] at (axis cs:1,0) {$\mu-2\sigma$};
      \node[below] at (axis cs:5,0) {$\mu+2\sigma$};
    \end{axis}
  \end{tikzpicture}
\end{center}

\begin{exemple}
  Nadia conçoit des jeux vidéos. Elle a programmé une potion de soin pour que celle-ci redonne un nombre de point de vie suivant une loi normale d'espérance 30 et d'écart-type 4. Marwane teste ce jeu : il boit une potion de soin. Combien de points de vie peut-il espérer gagner ?

  Puisque le nombre de points de vie gagnés suit une loi normale d'espérance $\mu=30$ et d'écart-type $\sigma=4$, son intervalle de fluctuation à 95\% est :
  \[
    \intervallef{\mu-2\sigma;\mu+2\sigma}
    =
    \intervallef{30-2\times4;30+2\times4}
    =
    \intervallef{22;38}
  \]
  Donc le personnage de Marwane va gagner entre 22 et 38 points de vie avec une probabilité de plus de 95\% (donc il y a moins de 5\% de chances qu'il gagne moins de 22 ou plus de 38 points de vie).
\end{exemple}

\begin{remarque}
La notion d'\emph{intervalle de confiance} n'est pas utilisée avec la loi normale en terminale \textsc{stmg}.
\end{remarque}

\section*{Exercices}
\begin{em}
  Essayez de faire les exercices sans regarder la solution, puis comparez vos résultats au corrigé (à la fin de ce document). Si des questions persistent, n'hésitez pas à me demander par courriel ou par l'ENT.
\end{em}

\begin{exercice}[Centres étrangers, 11 juin 2015]
Un laboratoire pharmaceutique fabrique des gélules contenant une substance S. La masse
de substance S, exprimée en milligrammes (mg), contenue dans une gélule est modélisée
par une variable aléatoire $X$ suivant la loi normale d'espérance $8,2$ et d'écart type $0,05$. 

La norme de fabrication impose que la masse de substance S dans une gélule soit comprise
entre 8,1~mg et 8,3~mg. La probabilité qu'une gélule soit hors norme après la fabrication
est :

\begin{tabularx}{\linewidth}{*{4}{X}}
\textbf{a.~~} 0,2&\textbf{b.~~} 0,05 &\textbf{c.~~}0,8 &\textbf{d.~~} 0,95
\end{tabularx}
\end{exercice}

\begin{exercice}[Métropole La Réunion, 16 juin 2016]
Un test d'aptitude est évalué sur $100$ points. Il faut obtenir au moins $60$~points pour le réussir.

Le score d'un candidat est modélisé par une variable aléatoire $X$ suivant une loi normale
d'espérance $\mu = 66$ et d'écart type $\sigma$ inconnu.

La probabilité, pour un candidat pris au hasard, d'obtenir un score compris entre $60$ et $72$
points est égale à $0,95$.

Parmi les valeurs ci-dessous, la plus proche de $\sigma$ est :

\begin{tabularx}{\linewidth}{*{4}{X}}
\textbf{a.~~} 3 &\textbf{b.~~} 6 &\textbf{c.~~} 5 &\textbf{d.~~} 9
\end{tabularx}
\end{exercice}

\section*{Corrigé}

\begin{exercice}[Centres étrangers, 11 juin 2015]
  On remarque que puisque la variable aléatoire $X$ suit une loi normale d'espérance $\mu=8,2$ et d'écart-type $\sigma=0,05$, l'intervalle $\left[ 8,1;8,3 \right]=\left[ 8,2-2\times0,05;8,2+2\times2,5 \right]=\left[ \mu-2\sigma;\mu+2\sigma \right]$ est l'intervalle à 95\% de la variable aléatoire.

  Donc la probabilité que la gélule soit à l'intérieur de cette norme est supérieure à 95\%, et la probabilité qu'elle soit à l'extérieur de cette norme est inférieure à 0,05\% (réponse \textbf{d.}).
\end{exercice}

\begin{exercice}[Métropole La Réunion, 16 juin 2016]
  Puisque la probabilité d'obtenir un score compris entre 60 et 72 points est égale à 0,95, l'intervalle $\left[ 60;72 \right]$, centré sur l'espérance $\mu=66$ est l'intervalle de fluctuation à 95\% de cette variable aléatoire. Donc
  $\left[ 60;72 \right]=\left[ \mu-2\sigma;\mu+2\sigma \right]$ et :
  \begin{align*}
    72&=\mu+2\sigma\\
    72&=66 +2\sigma\\
    72-66&=2\sigma\\
    6&=2\sigma\\
    3&=\sigma
  \end{align*}
  L'écart-type $\sigma$ est donc environ égal à 3 (réponse \textbf{a}).
\end{exercice}

\end{document}
