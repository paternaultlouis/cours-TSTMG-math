%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Copyright 2017 Louis Paternault --- http://ababsurdo.fr
%
% Publié sous licence Creative Commons Attribution-ShareAlike 4.0 International (CC BY-SA 4.0)
% http://creativecommons.org/licenses/by-sa/4.0/deed.fr
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% Pour compiler :
%$ lualatex $basename

\documentclass[12pt]{article}

\usepackage{1920-pablo}
\usepackage{1920-pablo-paternault}
\usepackage[a4paper,margin=1.5cm]{geometry}

\pagestyle{empty}
\setlength{\parindent}{0cm}

\begin{document}

\section{Modes de génération d'une suite}

\begin{definition}
Étant donné une suite $u$, le terme $u_n$ désigne le \emph{terme de rang} $n$.
\end{definition}

\begin{propriete}
Étant donné une suite $u$ :
\begin{itemize}
\item $u_{n-1}$ est le terme qui \blanc{précède} $u_n$.
\item $u_{n+1}$ est le terme qui \blanc{suit} $u_n$.
\end{itemize}
\end{propriete}

\begin{definition}Une suite est le plus souvent définie par :
\begin{description}
\item[\blanc{Récurrence} :] En fonction du terme précédent.
\item[\blanc{Formule explicite} :] Lorsque l'on peut calculer directement n'importe quel terme.
\end{description}
\end{definition}

\begin{exercice}
Calculer les trois premiers termes des suites suivantes.
\begin{enumerate}
\item $u$ définie sur $\mathbb{N}$ par $u_0=4$ et $u_{n+1}=2u_n-1$.
\item $v$ définie sur $\mathbb{N}^*$ par $u_n=3n+2$.
\end{enumerate}
\vfill
\end{exercice}


\section{Suites arithmétiques}

\begin{ndefinition}
  Une suite $u$ est dite \emph{arithmétique} s'il existe un réel $r$, appelé \blanc{raison}, tel que pour tout $n\in\mathbb{N}$, on ait : \blanc{$u_{n+1}=u_n+r$}.

  Habituellement, une suite arithmétique est définie par la donnée de \blanc{son premier terme et sa raison}.
\end{ndefinition}

\begin{npropriete}~
  \begin{itemize}
    \item Pour tout $n$ et $p$ de son domaine de définition, on a : \blanc{$u_n=u_p+(n-p)r$}.
    \item En particulier, si $u$ est définie sur $\mathbb{N}$, pour tout $n\in\mathbb{N}$, on a : \blanc{$u_n=u_0+nr$}.
  \end{itemize}
\end{npropriete}

\begin{npropriete}
  La suite $u$ est :
  \begin{itemize}
    \item (strictement) croissante si et seulement si \blanc{sa raison est}\\ \blanc{(strictement) positive ;}
    \item (strictement) décroissante si et seulement si \blanc{sa raison est}\\ \blanc{(strictement) négative ;}
    \item constante si \blanc{sa raison est nulle}.
  \end{itemize}
\end{npropriete}

\begin{exercice}
Cette année, les ventes d'un revendeur d'ordinateur ont diminué. Mais il s'était engagé avec son fournisseur, et il continue à recevoir chaque semaine 15 ordinateurs de plus qu'il n'en vend. Il loue un local pour stocker ces ordinateurs.

On appelle $\left(u_n\right)$ la suite modélisant le nombre d'ordinateurs stockés dans le local au bout de $n$ semaines. Ainsi, $u_1=0$ est le nombre d'ordinateurs stockés au début de la première semaine ; $u_2$ est le nombre d'ordinateurs stockés au début de la deuxième semaine, et ainsi de suite.

\begin{enumerate}
\item Justifier que la suite $u$ est arithmétique, et donner son premier terme et sa raison.
\item Calculer $u_2$, $u_3$, $u_4$.
\item Combien d'ordinateurs seront stockés au bout de 10 semaines ?
\item Au bout de combien de temps son local, qui permete de stocker 230 ordinateurs, sera-t-il plein ?
\end{enumerate}
\end{exercice}

% TODO
\pagebreak
\setcounter{ndefinition}{0}

\section{Suites géométriques}

\begin{definition}~
  Une suite $v$ est dite \emph{géométrique} s'il existe un réel $q$ non nul, appelé \blanc{raison}, tel que pour tout $n$ de son domaine de définition on ait : \blanc{$v_{n+1}=qv_n$.}
\end{definition}

\begin{nexemple}[Définition]~
  \begin{enumerate}[(a)]
    \item Les cinq premiers termes de la suite géométrique de premier terme 4 et de raison $\frac{1}{2}$ sont : \ldots
    \item La suite de termes $1/3$ ; 1 ; 3 ; 9 ; 27 ; etc. est géométrique de premier terme \ldots{} et de raison \ldots{}.
  \end{enumerate}
\end{nexemple}

\begin{propriete}[Terme général]~
  \begin{itemize}
    \item Pour tout $p$ et $n$ de son domaine de définition, on a : \blanc{$v_n=v_pq^{n-p}$}.
    \item En particulier, si $v$ est définie sur $\mathbb{N}$, pour tout $n\in\mathbb{N}$, on a : \blanc{$v_n=v_0q^n$.}
  \end{itemize}
\end{propriete}

\begin{nexemple}[Terme général]~
  \begin{enumerate}
    \item Soit $u$ la suite géométrique de
      premier terme $u_7=12$ et de raison $0,85$. Calculer $u_{10}$ et $v_{20}$.
    \item Soit $v$ la suite géométrique définie sur $\mathbb{N}$ par :
      \[\left\{\begin{array}{rcll}
        v_0 & = & 1729 \\
        v_{n+1} & = & 1.02v_n & \text{pour tout $n\geq7$}
    \end{array}\right.\]
      Calculer $v_{10}$ et $v_{100}$.
  \end{enumerate}
\end{nexemple}

\begin{propriete}[Variations]~
  Soit une suite géométrique de premier terme $v_0>0$ et de raison $q>0$. Alors :
  \begin{itemize}[$\bullet$]
    \item si $0<q<1$ : $v$ est \blanc{décroissante} ;
    \item si $q=1$ : $v$ est \blanc{constante} ;
    \item si $q>1$ : $v$ est \blanc{croissante}.
  \end{itemize}
\end{propriete}

\begin{nexemple}[Variations]
  Donner le sens de variations des suites de l'exemple précédent.
\end{nexemple}

\begin{exercice}[D'après le sujet de bac STMG Nouvelle Calédonie --- 16 novembre 2016]~

En janvier 2015, une entreprise renouvelle son parc de tablettes tactiles.

La tablette choisie affiche une autonomie de 8 heures. Une étude montre que l'autonomie de la batterie baisse de 15\,\% chaque année d'utilisation.

Soit $n$ un entier naturel. On modélise le nombre d'heures d'autonomie de cette tablette pour l'année $2015 + n$ par une suite $\left(u_n\right)$. Ainsi $u_0 = 8$.

\emph{On arrondira les résultats au centième d'heure.}

\begin{enumerate}
\item \begin{enumerate}
\item Vérifier que $u_1 = 6,8$.
\item Calculer $u_2$ et en donner une interprétation.
\end{enumerate}
\item Expliquer pourquoi la suite $\left(u_n\right)$ est géométrique. En donner sa raison.
\item Selon ce modèle, quelle sera l'autonomie de la tablette en janvier 2020 ?
\item L'entreprise souhaite prévoir le nombre d'années au bout desquelles l'autonomie sera
inférieure à quatre heures.

On considère l'algorithme suivant :
\begin{center}
\begin{tabular}{|lcl|}
\hline
\textbf{Initialisation}	&& $n$ prend la valeur 0\\
						&& $u$ prend la valeur 8\\
						&& $q$ prend la valeur 0,85\\
\textbf{Traitement}		&&Tant que $u > 4$\\
						&&\hspace{1.25em} $n$ prend la valeur $n + 1$\\
						&&\hspace{1.25em}$u$ prend la valeur $8 \times q$\\
						&&Fin tant que\\
\textbf{Sortie}			&& Afficher $n$\\
\hline
\end{tabular}
\end{center}

Quelle sera la valeur affichée en sortie ?
\end{enumerate}
\end{exercice}

\end{document}
