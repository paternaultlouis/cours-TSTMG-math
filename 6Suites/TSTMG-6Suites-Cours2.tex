%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Copyright 2020 Louis Paternault --- http://ababsurdo.fr
%
% Publié sous licence Creative Commons Attribution-ShareAlike 4.0 International (CC BY-SA 4.0)
% http://creativecommons.org/licenses/by-sa/4.0/deed.fr
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% Pour compiler :
%$ lualatex $basename

\documentclass[12pt]{article}

\usepackage{1920-pablo}
\usepackage{1920-pablo-paternault}
\usepackage[a5paper, bottom=1.0cm, left=.8cm, right=.8cm]{geometry}

\usepackage{fancyhdr}
\pagestyle{fancy}
\fancyhead[L]{\textsc{Chapitre 6 --- Suites}}
\fancyhead[R]{}
\fancyfoot[C]{}

\setlength{\parindent}{0pt}

% Point médian
\newcommand\pointmedian{\kern-0.25em\textperiodcentered\kern-0.25em}
\catcode`·=\active
\def·{\pointmedian}

\usepackage{tabularx}
\usepackage{mdframed}

\usepackage{qrcode}

\renewcommand{\blanc}[1]{\textcolor{red}{\large #1}}

\begin{document}

\setcounter{section}{1}
\section{Suite géométrique}

\begin{em}
  \begin{itemize}
    \item Recopiez dans votre cours la définition, les trois propriétés, les trois exemples. Les parties des exemples \framebox{encadrées} sont des explications supplémentaires que vous n'êtes pas obligé·e·s de recopier.
    \item Si vous ne comprenez pas, ne paniquez pas, et avancez un peu plus loin dans la fiche : il y a des liens vers des vidéos qui expliquent ce cours.
  \end{itemize}
\end{em}

\begin{definition}~
  Une suite $v$ est dite \emph{géométrique} s'il existe un réel $q$ non nul, appelé \blanc{raison}, tel que pour tout $n$ de son domaine de définition on ait : \blanc{$v_{n+1}=q\times v_n$.}
\end{definition}

\begin{nexemple}[Définition]~
  \begin{enumerate}[(a)]
    \item Les cinq premiers termes de la suite géométrique de premier terme 4 et de raison $\frac{1}{2}$ sont : \blanc{4 ; 2 ; 1 ; 0,5 ; 0,25}.

      \begin{mdframed}
      En effet :
      \begin{itemize}
        \item le premier terme est 4 ;
        \item le second terme est $\frac{1}{2}\times4=2$ ;
        \item le second terme est $\frac{1}{2}\times2=1$ ;
        \item le second terme est $\frac{1}{2}\times1=0,5$ ;
        \item le second terme est $\frac{1}{2}\times0,5=0,25$.
      \end{itemize}
    \end{mdframed}
  \item La suite de termes $1/3$ ; 1 ; 3 ; 9 ; 27 ; etc. est géométrique de premier terme \blanc{$1/3$} et de raison \blanc{3}.
    \begin{mdframed}
      En effet, pour passer de $1/3$ à 1, on a multiplié par 3 ; pour passer de 1 à 3, on a multiplié par 3 ; pour passer de 3 à 9, on a multiplié par 3 ; pour passer de 9 à 27, on a multiplié par 3 ; etc.
    \end{mdframed}
  \end{enumerate}
\end{nexemple}

\begin{propriete}[Terme général]~
  \begin{itemize}
    \item Pour tout $p$ et $n$ de son domaine de définition, on a : \blanc{$v_n=v_p\times q^{n-p}$}.
    \item En particulier, si $v$ est définie sur $\mathbb{N}$, pour tout $n\in\mathbb{N}$, on a : \blanc{$v_n=v_0\times q^n$.}
  \end{itemize}
\end{propriete}

\begin{em}
  \begin{tabular}{p{10cm}c}
  Une vidéo pour vous aider à comprendre cette propriété :
     \url{http://youtu.be/WTmdtbQpa0c}&
  \qrcode{http://youtu.be/WTmdtbQpa0c}
\end{tabular}
\end{em}


\begin{nexemple}[Terme général]~
  \begin{em}
  \begin{enumerate}
    \item Soit $u$ la suite géométrique de
      premier terme $u_7=12$ et de raison $0,85$. Calculer $u_{10}$ et $u_{20}$.
    \item Soit $v$ la suite géométrique définie sur $\mathbb{N}$ par :
      \[\left\{\begin{array}{rcll}
        v_0 & = & 1729 \\
        v_{n+1} & = & 1,02v_n & \text{pour tout $n\geq0$}
    \end{array}\right.\]
      Calculer $v_{10}$ et $v_{100}$.
  \end{enumerate}
\end{em}
\begin{enumerate}
  \item On applique la formule $u_n=u_p\times q^{n-p}$, où :
    \begin{description}
      \item[le premir terme] est $u_7$, donc $p=7$ ;
      \item[la raison]  est $0,85$ ;
      \item[le terme recherché]  est $u_{10}$, donc $n=10$.
    \end{description}
    Cela donne :
    \begin{align*}
      u_{10}&=u_7\times0,85^{10-7}\\
      &=12\times0,85^3\\
      &=7,3695
    \end{align*}

    De même, pour $u_{20}$, on applique la même formule avec $n=20$ :
    \begin{align*}
      u_{20}&=u_7\times0,85^{20-7}\\
      &=12\times0,85^13\\
      &\approx1,45
    \end{align*}
  \item On reconnait une suite géométrique de premier terme $v_0=1729$ et de raison $1,02$. Puisque l'indice du premier terme est 0, on peut appliquer le cas particulier $v_n=v_0\times q^n$, avec $q=1,02$. Donc :
    \begin{align*}
      v_{10}&=v_0\times1,02^{10}\\
      &=1729\times1,02^{10}\\
      &\approx 2107,64\\
      ~\\
      v_{100}&=v_0\times1,02^{100}\\
      &=1729\times1,02^{100}\\
      &\approx 12525,99
    \end{align*}
\end{enumerate}
\end{nexemple}

\begin{propriete}[Variations]~
  Soit une suite géométrique de premier terme $v_0>0$ et de raison $q>0$. Alors :
  \begin{itemize}[$\bullet$]
    \item si $0<q<1$ : $v$ est \blanc{décroissante} ;
    \item si $q=1$ : $v$ est \blanc{constante} ;
    \item si $q>1$ : $v$ est \blanc{croissante}.
  \end{itemize}
\end{propriete}

\begin{nexemple}[Variations]
  \emph{Donner le sens de variations des suites de l'exemple précédent.}
  \begin{enumerate}
    \item La première suite a pour coefficient directeur $0,85$, qui est strictement inférieur à 1 : elle est strictement décroissante.
    \item La deuxième suite a pour coefficient directeur $1,02$, qui est strictement supérieur à 1 : elle est strictement croissante.
  \end{enumerate}
\end{nexemple}

\pagebreak
\section*{Exercice corrigé}

\begin{em}
  Dans la partie exercice de votre cahier, faites cet exercice. Essayez de le faire sans regarder la correction, puis regardez-la pour vous aider si vous bloquez.
\end{em}

\begin{exercice}[D'après le sujet de bac STMG Nouvelle Calédonie --- 16 novembre 2016]~

En janvier 2015, une entreprise renouvelle son parc de tablettes tactiles.

La tablette choisie affiche une autonomie de 8 heures. Une étude montre que l'autonomie de la batterie baisse de 15\,\% chaque année d'utilisation.

Soit $n$ un entier naturel. On modélise le nombre d'heures d'autonomie de cette tablette pour l'année $2015 + n$ par une suite $\left(u_n\right)$. Ainsi $u_0 = 8$.

\emph{On arrondira les résultats au centième d'heure.}

\begin{enumerate}
\item \begin{enumerate}
\item Vérifier que $u_1 = 6,8$.
\item Calculer $u_2$ et en donner une interprétation.
\end{enumerate}
\item Expliquer pourquoi la suite $\left(u_n\right)$ est géométrique. En donner sa raison.
\item Selon ce modèle, quelle sera l'autonomie de la tablette en janvier 2020 ?
\item L'entreprise souhaite prévoir le nombre d'années au bout desquelles l'autonomie sera
inférieure à quatre heures.

On considère l'algorithme suivant :
\begin{center}
\begin{tabular}{|lcl|}
\hline
\textbf{Initialisation}	&& $n$ prend la valeur 0\\
						&& $u$ prend la valeur 8\\
						&& $q$ prend la valeur 0,85\\
\textbf{Traitement}		&&Tant que $u > 4$\\
						&&\hspace{1.25em} $n$ prend la valeur $n + 1$\\
						&&\hspace{1.25em}$u$ prend la valeur $8 \times q$\\
						&&Fin tant que\\
\textbf{Sortie}			&& Afficher $n$\\
\hline
\end{tabular}
\end{center}

Quelle sera la valeur affichée en sortie ?
\end{enumerate}
\end{exercice}

\begin{exercice}[Corrigé]~
  \begin{em}
En janvier 2015, une entreprise renouvelle son parc de tablettes tactiles.

La tablette choisie affiche une autonomie de 8 heures. Une étude montre que l'autonomie de la batterie baisse de 15\,\% chaque année d'utilisation.

Soit $n$ un entier naturel. On modélise le nombre d'heures d'autonomie de cette tablette pour l'année $2015 + n$ par une suite $\left(u_n\right)$. Ainsi $u_0 = 8$.
\end{em}

\emph{On arrondira les résultats au centième d'heure.}

\begin{enumerate}
\item \begin{enumerate}
    \item \emph{Vérifier que $u_1 = 6,8$.}

      Le nombre $u_1$ correspond à l'autonomie de la batterie l'année $2015+1$, soit en $2016$. L'autonomie initiale est de 8h, et l'autonomie baisse de 15\% chaque année, soit un coefficient multiplicateur de $1-\frac{15}{100}=0,85$. Donc $u_1=0,85\times8=6,8$.
    \item \emph{Calculer $u_2$ et en donner une interprétation.}
      De même, $u_2=0,85\times u_1=0,85\times6,8=5,78$. En 2017, la batterie aura une autonomie de 5,78 heures.
\end{enumerate}
\item \emph{Expliquer pourquoi la suite $\left(u_n\right)$ est géométrique. En donner sa raison.}

  Chaque année, l'autonomie de la batterie baisse de 15\%. Donc chaque terme de la suite est égal au précédent multiplié par le coefficient multiplicateur 0,85 : $u_{n+1}=0,85u_n$. On reconnait une suite géométrique de raison 0,85.
\item \emph{Selon ce modèle, quelle sera l'autonomie de la tablette en janvier 2020 ?}
  Janvier 2020 correspond à $2015+5$, donc $n=5$. Calculons $u_5$. Nous appliquons la formule $u_n=u_0\times q^n$ avec $q=0,85$ et $n=5$, donc :
  \[u_5=8\times0,85^5\approx3,55\]
  L'autonomie de la batterie en 2020 devrait être de 3,55 heures.
\item \emph{L'entreprise souhaite prévoir le nombre d'années au bout desquelles l'autonomie sera inférieure à quatre heures.}

On considère l'algorithme suivant :
\begin{center}
\begin{tabular}{|lcl|}
\hline
\textbf{Initialisation}	&& $n$ prend la valeur 0\\
						&& $u$ prend la valeur 8\\
						&& $q$ prend la valeur 0,85\\
\textbf{Traitement}		&&Tant que $u > 4$\\
						&&\hspace{1.25em} $n$ prend la valeur $n + 1$\\
						&&\hspace{1.25em} $u$ prend la valeur $8 \times q$\\
						&&Fin tant que\\
\textbf{Sortie}			&& Afficher $n$\\
\hline
\end{tabular}
\end{center}

\emph{Quelle sera la valeur affichée en sortie ?}
\begin{center}
  \begin{tabular}{rcScc}
  &$\boldsymbol{n}$
  &\multicolumn{1}{c}{$\boldsymbol{u}$}
  &$\boldsymbol{q}$
  &$\boldsymbol{u>4}$
  \\
  \hline
  Avant la boucle & 0 & 8 & 0,85 & Vrai \\
  1\iere{} itération & 1 & 6,8  & 0,85 & Vrai \\
  2\ieme{} itération & 2 & 5,78 & 0,85 & Vrai \\
  3\ieme{} itération & 3 & 4,91 & 0,85 & Vrai \\
  4\ieme{} itération & 4 & 4,18 & 0,85 & Vrai \\
  5\ieme{} itération & 5 & 3,55 & 0,85 & Faux \\
\end{tabular}
\end{center}
La valeur affichée en sortie sera la valeur de $n$, c'est-à-dire $n=5$.
\end{enumerate}
\end{exercice}

\vfill
\section*{Bilan}

Faire l'évaluation \texttt{bac-geometrique.pdf}, et me la rendre sur l'ENT ou par courriel.

\end{document}
